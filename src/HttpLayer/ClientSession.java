package HttpLayer;
import BusinessLogic.RecordLogic;
import DataLayer.UserGateway;
import ServiceLayer.LoginService;
import ServiceLayer.RecordsService;
import com.eaio.uuid.UUID;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;

/**
 * * Обрабатывает запрос клиента.
 */
public class ClientSession implements Runnable {
    RecordsService service = new RecordsService();
    LoginService logserv = new LoginService();
    UserGateway usrmapper = new UserGateway();
    
    @Override
    public void run() {
        try { /* Получаем заголовок сообщения от клиента */ 
            String header = readHeader();
            System.out.println(header + "\n"); 
            /* Получаем из заголовка указатель на интересующий ресурс */
            String usr = getUserNameFromHeader(header);
            System.out.println("User: " + usr + "\n"); 
            int code=404; // по умолчанию - не найден 
            // получаем id и дневник указанного пользователя
            if(logserv.checkLogin(usr)==true) {
                UUID id = usrmapper.findIdByUserName(usr);
                if(id!=null){
                    String[][] diary = RecordLogic.getDiaryToExport(id);
                    /* Отправляем содержимое ресурса клиенту */
                    // code = send(service.resultProduct);
                     code = send(diary);
                } else code = send(null);
            } else code = send(null);
            System.out.println("Result code: " + code + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ClientSession(Socket socket) throws IOException {
        this.socket = socket;
        initialize();
    }

    private void initialize() throws IOException { 
        /* Получаем поток ввода, в который помещаются сообщения от клиента */ 
        in = socket.getInputStream(); 
        /* Получаем поток вывода, для отправки сообщений клиенту */ 
        out = socket.getOutputStream();
    }

    /**
     * Считывает заголовок сообщения от клиента. 
     * @return строка с заголовком сообщения от клиента. 
     * @throws IOException
     */
    private String readHeader() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder builder = new StringBuilder();
        String ln = null;
        while (true) {
            ln = reader.readLine();
            if (ln == null || ln.isEmpty()) {
                break;
            }
            builder.append(ln + System.getProperty("line.separator"));
        }
        return builder.toString();
    }

    /**
     * Получаем из заголовка запроса - имя пользователя
     * @param header * заголовок сообщения от клиента. *
     * @return идентификатор ресурса.
     */
    private String getUserNameFromHeader(String header) {
        int from = header.indexOf(" ") + 1;
        int to = header.indexOf(" ", from);
        String userName = header.substring(from+1, to);
        int paramIndex = userName.indexOf("/");
        if (paramIndex != -1) {
            userName = userName.substring(0, paramIndex);
        }
        return userName;
    }

    /**
     * * Отправляет ответ клиенту. В качестве ответа отправляется http заголовок и 
     * содержимое указанного ресурса. Если ресурс не указан, отправляется 
     * перечень доступных ресурсов. 
     * 
     * @param diary - массив типа String[][] - содержит в себе дневник пользователя
     * @return код ответа. 200 - если ресурс был найден, 404 - если нет. 
     * @throws IOException
     */
    private int send(String[][] diary) throws IOException {
        // проверяем - не пуст ли дневник (существует ли он)
        int code;
        if(diary != null) code = 200;
        else {
            code = 404; return code;
        } 
        // если существует - заполняем его для вывода
       /* String ans="Дата"+"\t"+"Название"+"\t"+"Количество"+"\t"+"Белки"+"\t"+"Жиры"
                +"\t"+"Углеводы"+"\t"+"Ккал"+"\n";*/
        String ans="";
        for(String[] row: diary){
            for(String r: row)  {
                if(row[1]==null) break;
                ans+=r+" ";
            }
            if(row[1]==null) continue;
            ans+="\n";
        }         
        String header = getHeader(code); // "UTF-8"
        PrintStream answer = new PrintStream(out, true, "windows-1251");
        answer.print(header);
        if (code == 200) {
            byte[] b = ans.getBytes(Charset.forName("windows-1251"));
            out.write(b, 0, b.length);
        }
        return code;
    }

    /**
     * * Возвращает http заголовок ответа. * * @param code * код результата
     * отправки. * @return http заголовок ответа.
     */
    private String getHeader(int code) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("HTTP/1.1 " + code + " " + getAnswer(code) + "\n");
        buffer.append("Date: " + new Date().toGMTString() + "\n");
        buffer.append("Accept-Ranges: none\n");
        buffer.append("\n");
        return buffer.toString();
    }

    /**
     * * Возвращает комментарий к коду результата отправки. * * @param code *
     * код результата отправки. * @return комментарий к коду результата
     * отправки.
     */
    private String getAnswer(int code) {
        switch (code) {
            case 200:
                return "OK";
            case 404:
                return "Not Found";
            default:
                return "Internal Server Error";
        }
    }
    private Socket socket;
    private InputStream in = null;
    private OutputStream out = null;
   
}
