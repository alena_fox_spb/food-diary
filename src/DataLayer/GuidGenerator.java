package DataLayer;

// библиотека генератора уникальных id 
import com.eaio.uuid.UUID;

/**
 *
 * @author Alena_Fox
 */
public class GuidGenerator {
    private static GuidGenerator generator = new GuidGenerator();
    
    private GuidGenerator(){
        
    }
    public static GuidGenerator getInstance(){
        return generator;
    }
    
    // для предотвращения конфликтов в неск-х потоках - synchronized
    public synchronized UUID genID(){
        return new UUID();
    }

}
