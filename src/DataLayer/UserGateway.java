package DataLayer;

import Entities.User;
import com.eaio.uuid.UUID;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Mapper between  USER and LOGPASS tables in JAVA DB and Buisness logic package.
 * @author Alena_Fox
 */
public class UserGateway {
    private GuidGenerator generatorID = GuidGenerator.getInstance();
    private Connection db = ConnectToBD.createConnection().getConnToBD();
    
    public synchronized int insertNewUsr(User usr) throws SQLException {
        if(usr==null) return -1;
        String statement = "INSERT INTO APP.USERS (USR_ISADMIN, USR_MALE, USR_AGE, USR_HEIGHT, USR_WEIGHT, USR_EXERCISELEVEL, USR_ID) VALUES (?, ?, ?, ?, ?, ?, ? )";
        insertUsrInfo(usr, statement);
        // также необходимо добавить запись с новым логин-паролем в др таблицу в БД
        insertLoginPass(usr);
        return 0;
    }
    
    public synchronized int updateUsrInfo(User usr) throws SQLException {
       if(usr==null) return -1;
        String statement = "UPDATE APP.USERS SET USR_ISADMIN = ?, USR_MALE = ?, USR_AGE = ?, USR_HEIGHT = ?, USR_WEIGHT = ?, USR_EXERCISELEVEL = ? where USR_ID = ?";
        insertUsrInfo(usr, statement);
        return 0;
    }
    
    private synchronized int insertUsrInfo(User usr, String statement) {
	if(usr==null||statement==null||statement.equals("")) return -1;
        try {
			PreparedStatement dbStatement = db.prepareStatement(statement);
                        dbStatement.setBoolean(1, usr.isIsAdmin());
                        dbStatement.setBoolean(2, usr.getMale());
                        dbStatement.setInt(3, usr.getAge());
                        dbStatement.setInt(4, usr.getHeight());
                        dbStatement.setInt(5, usr.getWeight());
                        dbStatement.setInt(6, usr.getExerсiseLevel());
                        
                        // если id не был назначен принудительно у объекта, то генерируем его перед вставкой
                        UUID guid = usr.getId();
                        if (guid == null) guid = generatorID.genID();
                        dbStatement.setString(7, guid.toString());
                        usr.setId(guid);
                        
			dbStatement.executeUpdate();	    
                        
		} catch (SQLException e) {  			
                   return -1;
		}
        return 0;
	}
    public synchronized int insertLoginPass(User usr){
     if(usr==null) return -1;
        try{   
                String statement = "INSERT INTO APP.LOGPASS (LOGIN, PASSWORD, USR_ID) VALUES (?, ?, ?)";
                PreparedStatement dbStatement = db.prepareStatement(statement);
                dbStatement.setString(1,usr.getLogin());
                dbStatement.setString(2,usr.getPassword());
                dbStatement.setString(3,usr.getId().toString());

                dbStatement.executeUpdate();
            } catch (SQLException e) {  			
                   return -1;
		}
        return 0;
    }
    
    
    public synchronized User findbyID(UUID id) throws SQLException {
        if(id==null) return null;
        // ищем пользователя в таблице USERS
        String statement = "select USR_ISADMIN, USR_MALE, USR_AGE, USR_HEIGHT, USR_WEIGHT, USR_EXERCISELEVEL from APP.USERS where USR_ID = ?";
        User usr = find(id, statement);
        // ищем логин-пароль польхователя в таблице LOGPASS
        ResultSet rs = findLoginByUserID(id);
        parseLogPassResultSet(rs, usr);
        
        return usr;
    }
    
    private synchronized User find(UUID id, String statement)  {
        if(id==null||statement==null||statement.equals("")) return null;
            try {
                    PreparedStatement dbStatement = db.prepareStatement(statement);
                    dbStatement.setString(1, id.toString());
                    ResultSet rs = dbStatement.executeQuery();
                    // разбираем полученные из БД результаты
                    User usr = new User(null, null);
                    usr = parseUserResultSet(rs, id); 
                    
                    return usr;

            } catch (Exception e) {
                 return null;
            }
    }
	
    public synchronized boolean isAdminUser(UUID usrId){
       if(usrId==null) return false;
        String statement =  "select usr_isadmin from APP.users where usr_id = ?";
        PreparedStatement dbStatement;
        try {
            dbStatement = db.prepareStatement(statement);        
            dbStatement.setString(1, usrId.toString());
            ResultSet rs = dbStatement.executeQuery();
            while(rs.next()) {
                return rs.getBoolean("usr_isadmin");               
            }
        } catch (SQLException ex) {
           return false;
        }
        return false;
    }
    
    public synchronized UUID findIdByUserName(String name){
        if(name==null||name.equals("")) return null;
        String statement =  "select usr_id from APP.LOGPASS where login = ?";
        PreparedStatement dbStatement;
        try {
            dbStatement = db.prepareStatement(statement);        
            dbStatement.setString(1, name);
            ResultSet rs = dbStatement.executeQuery();
            while(rs.next()) {
                return new UUID(rs.getString("usr_id"));               
            }
        } catch (SQLException ex) {
           return null;
        }
        // если не было найдено пользовтеля - возвращаем null 
        return null;
    }
    /**
     * Ищем в таблице LOGPASS в БД логин и пароль пользователя с конкретным id
     * @param id
     * @return
     * @throws SQLException 
     */
    public synchronized ResultSet findLoginByUserID(UUID id)  {
        if(id==null) return null;
        try {
                String statement = "select LOGIN, PASSWORD from APP.LOGPASS where USR_ID = ?";
                PreparedStatement dbStatement = db.prepareStatement(statement);
                dbStatement.setString(1, id.toString());
                return dbStatement.executeQuery();              
                    
            }catch (SQLException e) {
                 return null;
            }
    }
    /**
     * Смотрим таблицу LOGPASS - есть ли в ней такой логин
     * @param login 
     * @return true - если существует в БД такой логин
     * @throws SQLException 
     */
    public synchronized boolean loginExists(String login) {
        if(login==null||login.equals("")) return false;
        try {
                String statement = "select * from APP.LOGPASS where LOGIN = ?";
                PreparedStatement dbStatement = db.prepareStatement(statement);
                dbStatement.setString(1, login);
                ResultSet rs = dbStatement.executeQuery();
                return rs.next();
                    
            }catch (SQLException e) {
                return false;
            }
    }
    
    /**
     * Смотрим таблицу LOGPASS - проверяем наличие пары логин-пароль
     * @param login 
     * @return id пользователя - если существует в БД такой логин
     * null - если такого нет
     * @throws SQLException 
     */
    public synchronized UUID checkLoginPass(String login, String pass)  {
        if(login==null||login.equals("")||pass==null||pass.equals("")) return null;
        try {
                String statement = "select usr_id from APP.LOGPASS where LOGIN = ? and password = ?";
                PreparedStatement dbStatement = db.prepareStatement(statement);
                dbStatement.setString(1, login);
                dbStatement.setString(2, pass);
                ResultSet rs = dbStatement.executeQuery();
                if (rs.next()) return new UUID(rs.getString("usr_id"));
                else return null;
                    
            }catch (SQLException e) {
                 return null;
            }
    }
    
    /**
     * Разбираем результат от таблицы USER (БД) и возвращаем объект User
     * @param rs
     * @param id
     * @return 
     */
    private User parseUserResultSet (ResultSet rs, UUID id){
        try {
            while(rs.next()) {
                User usr = new User(null, null);                
                usr.setDataAboutUser(rs.getBoolean("USR_MALE"),
                        rs.getInt("USR_AGE"), rs.getInt("USR_Height"), rs.getInt("USR_weight"),
                        rs.getInt("USR_EXERCISELEVEL"));
                usr.setIsAdmin(rs.getBoolean("USR_ISADMIN"));
                usr.setId(id);
                return usr;
            }
        } catch (Exception ex) {
           return null;
        }
        return null;
    }
    
    /**
     * Разбираем результат от таблицы LOGPASS (БД) и возвращаем объект User c установленными 
     * полями логин, пароль
     * @param rs
     * @param id пользователя 
     * @return 
     */
    private User parseLogPassResultSet (ResultSet rs, User usr){
        try {
            while(rs.next()) {                
                usr.setLogin(rs.getString("LOGIN"));
                usr.setPassword(rs.getString("PASSWORD"));
                return usr;
            }
        } catch (SQLException ex) {
            return null;
        }
        return null;
    }
}
