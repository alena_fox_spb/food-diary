package DataLayer;

import Entities.Exercise;
import com.eaio.uuid.UUID;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Mapper between EXERCISES table in JAVA DB and Buisness logic package.
 *
 * @author Alena_Fox
 */
public class ExercisesGateway {

    private GuidGenerator generatorID = GuidGenerator.getInstance();
    private Connection db = ConnectToBD.createConnection().getConnToBD();

    public synchronized int insert(Exercise ex) throws SQLException {
        if (ex == null) {
            return -1;
        }
        try {
            String statement = "INSERT INTO APP.EXERCISES (ex_name, ex_kkal_per_hour , ex_id) VALUES (?,?,?)";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setString(1, ex.getName());
            dbStatement.setInt(2, ex.getKkalPerHour());
            // если id не был назначен принудительно у объекта Exercise, то генерируем его перед вставкой
            UUID guid = ex.getId();
            if (guid == null) {
                guid = generatorID.genID();
            }
            dbStatement.setString(3, guid.toString());

            dbStatement.executeUpdate();

        } catch (SQLException e) {
            return -1;
        }
        return 0;
    }

    public synchronized List<Exercise> findAll() {
        try {
            String statement = "select * from APP.EXERCISES";
            PreparedStatement dbStatement = db.prepareStatement(statement);

            ResultSet rs = dbStatement.executeQuery();
            List<Exercise> catalog = new ArrayList<>();
            while (rs.next()) {
                Exercise ex = parseResultSet(rs);
                catalog.add(ex);
            }
            return catalog;

        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * Find and return ONE Exercise object if it exist in DB
     *
     * @param paramValue
     * @param statement
     * @return
     * @throws SQLException
     */
    private synchronized Exercise find(String paramValue, String statement) {
       if(paramValue==null||statement==null) return null;
        try {
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setString(1, paramValue);
            ResultSet rs = dbStatement.executeQuery();
            return parseResultSet(rs);

        } catch (SQLException e) {
            return null;
        }
    }

    public synchronized Exercise findByName(String name)  {
       if(name==null) return null;
        try {
            String statement = "select EX_NAME, EX_ID, EX_KKAL_PER_HOUR from APP.EXERCISES WHERE EX_NAME = ? ";
            return find(name, statement);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Find one exercise with ID in DB
     *
     * @param uniqueID
     * @return
     * @throws SQLException
     */
    public synchronized Exercise findByGuId(UUID uniqueID)  {
       if(uniqueID==null) return null;
        try {
            String statement = "select EX_NAME, EX_ID, EX_KKAL_PER_HOUR from APP.EXERCISES WHERE EX_ID = ? ";
            return find(uniqueID.toString(), statement);

        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Parse ResultSet object with one Exercise Object in it
     *
     * @param rs
     * @return
     */
    private Exercise parseResultSet(ResultSet rs) {
        try {
            while (rs.next()) {
                Exercise ex = new Exercise();
                ex.setId(new UUID(rs.getString("EX_ID")));
                ex.setName(rs.getString("EX_NAME"));
                ex.setKkalPerHour(rs.getInt("EX_KKAL_PER_HOUR"));
                return ex;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ExercisesGateway.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
