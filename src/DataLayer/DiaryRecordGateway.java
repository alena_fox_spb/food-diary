package DataLayer;

import Entities.RecordInDairy;
import com.eaio.uuid.UUID;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Mapper between  DIARY table in JAVA DB and Buisness logic package.
 * @author Alena_Fox
 */
public class DiaryRecordGateway {
    private GuidGenerator generatorID = GuidGenerator.getInstance();
    private Connection db = ConnectToBD.createConnection().getConnToBD();
    
    public synchronized int insert(RecordInDairy rec, UUID userID) throws SQLException {
	if(userID==null || rec ==null) return -1;
        try {
			String statement = "INSERT INTO APP.DIARY (D_DATE, D_CUR_WEIGHT, D_NUMOFPRODUCTOREX, PROD_ID, EX_ID, USR_ID, D_ID) VALUES (?,?,?,?,?,?,?)";
			PreparedStatement dbStatement = db.prepareStatement(statement);
                        java.util.Date date = rec.getDateAndTIme();
                        java.sql.Timestamp sqldate = new java.sql.Timestamp(date.getTime());
                        
                        dbStatement.setTimestamp(1, sqldate);
                        
                        if(rec.getType()==RecordInDairy.typeOfRec.WEIGHT){
                            dbStatement.setDouble(2, rec.getCurWeight());
                            dbStatement.setInt(3, 0);
                            dbStatement.setString(4,null);
                            dbStatement.setString(5,null);
                        }
                        else if(rec.getType()==RecordInDairy.typeOfRec.EXERCISE){
                            dbStatement.setDouble(3, rec.getNumOfProductOrExercise());
                            dbStatement.setString(5, rec.getExercise().getId().toString());
                            dbStatement.setString(4,null);
                            dbStatement.setInt(2, 0);
                        }
                        else if(rec.getType()==RecordInDairy.typeOfRec.PRODUCT){
                            dbStatement.setDouble(3, rec.getNumOfProductOrExercise());
                            dbStatement.setString(4, rec.getProduct().getId().toString());
                            dbStatement.setInt(2, 0);
                            dbStatement.setString(5,null);
                        }
                                                
                        dbStatement.setString(6, userID.toString());
                        // если id не был назначен принудительно у объекта DIARY то генерируем его перед вставкой
                        UUID guid = rec.getId();
                        if (guid == null) guid = generatorID.genID();
                        dbStatement.setString(7, guid.toString());
                        
			dbStatement.executeUpdate();			
		} catch (SQLException e) {  			
                    return -1;                   
		}
        return 0;
    }
    
    /**
     * Поиск записи по ее id 
     * @param recID
     * @return
     * @throws SQLException 
     */
    public synchronized RecordInDairy findbyRecID(UUID recID) throws SQLException {
        if(recID==null) return null;
        String statement = "select D_DATE, D_CUR_WEIGHT, D_NUMOFPRODUCTOREX, PROD_ID, EX_ID, USR_ID from APP.DIARY where D_ID = ? ";
        return find(recID, statement);
    }
    
    /**
     * Поиск всех записей по id пользователя 
     * @param usrID
     * @return List<RecordInDairy>
     * @throws SQLException 
     */
    public synchronized List<RecordInDairy> findbyAllRecordsByUsrID(UUID usrID)  {
       if(usrID==null) return null;
       String statement = "select D_DATE, D_CUR_WEIGHT, D_NUMOFPRODUCTOREX, PROD_ID, EX_ID, D_ID from APP.DIARY where USR_ID = ? order by D_DATE";
       try {
                    List<RecordInDairy> listOfRecs = new ArrayList<RecordInDairy>();
                    
                    PreparedStatement dbStatement = db.prepareStatement(statement);
                    dbStatement.setString(1, usrID.toString());
                    ResultSet rs = dbStatement.executeQuery();
                    // разбираем полученные из БД результаты
                    while(rs.next()) {
                        listOfRecs.add(parseResultSet(rs, usrID));
                    }
                    return listOfRecs;

            } catch (SQLException e) {
                return null;
            }
    }      
    
    public synchronized List<RecordInDairy> findRecordsByDate(UUID usrID, Date date){
       if(usrID==null||date==null) return null;
       try {
                    List<RecordInDairy> listOfRecs = new ArrayList<RecordInDairy>();
                    // подготавливаем дату к виду '2014-05-08 00:00:00.0' (т.е. дата и 0 часов)
                    // собираем все в одну дату
                    Calendar calendar=Calendar.getInstance();
                    calendar.setTime(date);
                    //устаналиваем всё в одну дату-время (для удобства поиска\сравнения по конкр дню)
                    calendar.set(Calendar.HOUR_OF_DAY, 0 );
                    calendar.set(Calendar.MINUTE, 0 );
                    calendar.set(Calendar.SECOND, 0 );
                    Date realdate=calendar.getTime();
                    
                    java.sql.Timestamp sqldate = new java.sql.Timestamp(realdate.getTime());
                    
                    String statement = "select D_DATE, D_CUR_WEIGHT, D_NUMOFPRODUCTOREX, PROD_ID, EX_ID, D_ID from APP.DIARY where USR_ID = ? and DATE(D_DATE) =  DATE(TIMESTAMP('" +sqldate +"')) order by D_DATE";                   
                    PreparedStatement dbStatement = db.prepareStatement(statement);
                    dbStatement.setString(1, usrID.toString());
//                    dbStatement.setTimestamp(2, sqldate);
                    ResultSet rs = dbStatement.executeQuery();
                    // разбираем полученные из БД результаты
                    while(rs.next()) {
                        listOfRecs.add(parseResultSet(rs, usrID));
                    }
                    return listOfRecs;

            } catch (SQLException e) {
                return null;
            }
    } 
    
    private synchronized RecordInDairy find(UUID id, String statement) {
        if(id==null||statement==null)    return null;
        try {
                PreparedStatement dbStatement = db.prepareStatement(statement);
                dbStatement.setString(1, id.toString());
                ResultSet rs = dbStatement.executeQuery();
                // разбираем полученные из БД результаты
                return parseResultSet(rs, id);

            } catch (SQLException e) {
                   return null;
            }
    }
	
    private RecordInDairy parseResultSet (ResultSet rs, UUID id){
        try {    
                RecordInDairy prod = new RecordInDairy(rs.getTimestamp("D_DATE"), null);
                prod.setNumOfProductOrExercise(rs.getDouble("D_NUMOFPRODUCTOREX"));
                
                double weight = 0; String uids=null;
                weight = rs.getDouble("D_CUR_WEIGHT");
                if(weight!=0) {
                    prod.setType(RecordInDairy.typeOfRec.WEIGHT);
                    prod.setCurWeight(weight);
                }
                else if((uids=rs.getString("PROD_ID"))!=null) {
                    prod.setType(RecordInDairy.typeOfRec.PRODUCT);
                    ProductsGateway prmap = new ProductsGateway();
                    prod.setProduct(prmap.findByGuId(new UUID(uids)));
                }
                else if((uids=rs.getString("EX_ID"))!=null){
                    prod.setType(RecordInDairy.typeOfRec.EXERCISE);
                    ExercisesGateway exmap = new ExercisesGateway();
                    prod.setExercise(exmap.findByGuId(new UUID(uids)));
                }
                prod.setId(id);
                return prod;
        } catch (SQLException ex) {
            Logger.getLogger(DiaryRecordGateway.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
   
}
