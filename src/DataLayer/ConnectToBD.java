package DataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alena_Fox
 */
public class ConnectToBD {
    //URL of Oracle database server
    private static final String url = "jdbc:derby://localhost:1527/FoodDiary"; 
    private static final String user = "alena"; 
    private static final String pass = "alena777"; 
    // singleton 
    private static ConnectToBD connection = new ConnectToBD();
    // connection to DB
    private Connection conn;
    
    private ConnectToBD(){        
        //properties for creating connection to Oracle database
        Properties props = new Properties();
        props.setProperty("user",user) ;
        props.setProperty("password", pass);      
        try {
            //creating connection to Oracle database using JDBC
            conn = DriverManager.getConnection(url, props);
            System.out.println("ok, DataBase is connected\n");
        } catch (SQLException ex) {
            Logger.getLogger(ConnectToBD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static synchronized ConnectToBD createConnection(){
        return connection;
    }
    public synchronized Connection getConnToBD(){
         return conn;            
    }
}
