package DataLayer;

import Entities.Products;
import com.eaio.uuid.UUID;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Mapper between Products table in JAVA DB and Buisness logic package.
 * @author Alena_Fox
 */
public class ProductsGateway {
    private GuidGenerator generatorID = GuidGenerator.getInstance();
    private Connection db = ConnectToBD.createConnection().getConnToBD();
    
    public int checkExistProductAndInsertOrUpdateIt(Products prod) {
    if(prod==null)    return -1;
    // сначала проверяем -есть ли продукт с таким именем уже в бд
         try {
            Products findProd = null;
            findProd = findByName(prod.getName());
            if(findProd!=null)      { // продукт есть в базе - надо его обновить
                prod.setId(findProd.getId()); // устаналиваем его ID 
                update(prod);
            }
            else{ // продукта не найдено
                insert(prod);
            }
            } catch (Exception e) {
                return -1;
            }
         return 0;
    }
    public synchronized int insert(Products prod)  {
        if(prod==null)    return -1;
        try {
            String statement = "INSERT INTO APP.PRODUCTS ( PROD_KKAL, PROD_PROTEINS, PROD_FATS, PROD_CARBS, PR_IS_FOOD, PROD_ID,  PROD_NAME) VALUES (?,?,?,?,?,?,?)";
            doInsertOrUpdate(prod, statement);
        } catch (SQLException e) {
               return -1;
        }
        return 0;
    } // UPDATE Profile SET ModTime = '2007-04-01 23:59:59.999'"     + " WHERE ID > 6"
    
    public synchronized int update(Products prod) {
        if(prod==null)    return -1;
        try {
            String statement = "UPDATE APP.PRODUCTS SET PROD_KKAL= ?, PROD_PROTEINS=?, PROD_FATS=?,"
                    + " PROD_CARBS=?, PR_IS_FOOD=?, PROD_ID=? WHERE PROD_NAME = ?";
            doInsertOrUpdate(prod, statement);
        } catch (SQLException e) {
               return -1;
        }
        return 0;
    }
    
    private synchronized void doInsertOrUpdate(Products prod, String statement) throws SQLException{
        PreparedStatement dbStatement = db.prepareStatement(statement);
        dbStatement.setString(7, prod.getName());
        dbStatement.setInt(1, prod.getKkal());
        dbStatement.setInt(2, prod.getProteins());
        dbStatement.setInt(3, prod.getFats());
        dbStatement.setInt(4, prod.getCarbons());
        dbStatement.setBoolean(5, prod.isIsFood());
        // если id не был назначен принудительно у объекта Products, то генерируем его перед вставкой
        UUID guid = prod.getId();
        if (guid == null) guid = generatorID.genID();
        dbStatement.setString(6, guid.toString());
        dbStatement.executeUpdate();
    }

    public synchronized List<Products> findAll()  {
            try {
                    String statement = "select * from APP.PRODUCTS order by prod_name";
                    PreparedStatement dbStatement = db.prepareStatement(statement);
                    
                    ResultSet rs = dbStatement.executeQuery();
                    List<Products> catalog = new ArrayList<>();
                    while(rs.next()){
                        Products ex = parseResultSet(rs);
                        catalog.add(ex);                               
                    }
                    return catalog;

            } catch (SQLException e) {
                 return null;
            }
    }
    
    private synchronized Products find(String paramValue, String statement) {
       if(paramValue==null||statement==null) return null;
        try {
                    PreparedStatement dbStatement = db.prepareStatement(statement);
                    dbStatement.setString(1, paramValue);
                    ResultSet rs = dbStatement.executeQuery();
                    Products prodd= null;
                    while(rs.next()){
                        prodd = new Products();
                        prodd = parseResultSet(rs);
                    }
                    return prodd;

            } catch (SQLException e) {
             return null;
            }
    }
    
    public synchronized Products findByName(String name){
        if(name==null||name.equals(""))     return null;
        try {
                String statement = "select PROD_ID, PROD_NAME, PROD_KKAL, PROD_PROTEINS, PROD_FATS, PROD_CARBS, PR_IS_FOOD from APP.PRODUCTS WHERE PROD_NAME = ? ";
                return find(name, statement);

            } catch (Exception e) {
                    return null;
            }
    }
    
    public synchronized Products findByGuId(UUID uniqueID) {
        if(uniqueID==null)     return null;
        try {
                    String statement = "select PROD_NAME, PROD_KKAL, PROD_PROTEINS, PROD_FATS, PROD_CARBS, PR_IS_FOOD, PROD_ID from APP.PRODUCTS WHERE PROD_ID = ? ";
                    return find(uniqueID.toString(), statement);

            } catch (Exception e) {
                    return null;
            }
    }

    private Products parseResultSet(ResultSet rs) {
        try {
                UUID guid = new UUID(rs.getString("PROD_ID"));
                Products prod = new Products(rs.getString("PROD_NAME"), rs.getBoolean("pr_is_food"),
                        rs.getInt("prod_kkal"), rs.getInt("prod_proteins"),rs.getInt("prod_fats"),
                        rs.getInt("prod_carbs"));
                prod.setId(guid);
                return prod;
            
        } catch (SQLException ex) {
            Logger.getLogger(ProductsGateway.class.getName()).log(Level.SEVERE, null, ex);
        }
       return null;
    }
	
}
