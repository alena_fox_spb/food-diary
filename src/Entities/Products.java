package Entities;

import com.eaio.uuid.UUID;


// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.5B1E4F16-C3DD-FB19-22A6-DAD2979599FF]
// </editor-fold> 
public class Products {

        private UUID id;

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public UUID getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(UUID id) {
        this.id = id;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.8B6A752D-43DD-675E-BA5E-884911F1148D]
    // </editor-fold> 
    private String Name;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.58E54988-A2DF-8D58-4A27-A8A1BF4905ED]
    // </editor-fold> 
    private int Proteins;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.2D46F558-C33A-BE97-5F57-A28FCC01DAAD]
    // </editor-fold> 
    private int Fats;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.D73EA780-D113-D84A-B085-2D959E74069F]
    // </editor-fold> 
    private int Carbons;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.00D229BE-39AB-5FAC-E8FD-A62A0B329A93]
    // </editor-fold> 
    private int Kkal;

    /**
     * isFood = true - mean that Product is a food, 
     * isFood = false - mean that Product is a drink
     */
    private boolean isFood;

    /**
     * 
     * @return 'isFood' - true - mean that Product is a food, false - mean that Product is a drink
     */
    public boolean isIsFood() {
        return isFood;
    }

    /**
     * 
     * @param isFood - true - mean that Product is a food, false - mean that Product is a drink
     */
    public void setIsFood(boolean isFood) {
        this.isFood = isFood;
    }
       
   
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.80A84B66-189D-2008-5868-72243B70E88D]
    // </editor-fold> 
    public Products (String Name, boolean isFood, int Kkal, int P, int F, int C) {
        this.Name = Name;
        this.isFood = isFood;
        this.Kkal = Kkal;
        this.Proteins = P;
        this.Carbons = C;
        this.Fats = F;
    }

    /**
     * Default creating of object Product. All numeric fields are '-1', isProduct = true, Name = 'name'
     */
    public Products(){
        this.Name = "name";
        this.isFood = true;
        this.Kkal = -1;
        this.Proteins = -1;
        this.Carbons = -1;
        this.Fats = -1;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.4B67D008-D65D-8139-9456-2037DC11C781]
    // </editor-fold> 
    public int getCarbons () {
        return Carbons;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.D15882A5-A993-8031-A710-FBF2389E80B4]
    // </editor-fold> 
    public void setCarbons (int val) {
        this.Carbons = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.594A44E0-B0DB-D113-3D39-D8AF437E2864]
    // </editor-fold> 
    public int getFats () {
        return Fats;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.92F43738-E77F-D3D1-BDAF-D6BC009FE1F9]
    // </editor-fold> 
    public void setFats (int val) {
        this.Fats = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.C6325923-DC50-1D65-248E-739DDD244174]
    // </editor-fold> 
    public int getKkal () {
        return Kkal;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.33ED1DC1-AD06-1C8B-8DB0-027F2B97DFFE]
    // </editor-fold> 
    public void setKkal (int val) {
        this.Kkal = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.A8489564-26B0-38AD-4A1C-D8046940EB78]
    // </editor-fold> 
    public String getName () {
        return Name;
    }


    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.41B0C739-AF00-401E-15EE-C9A88861E803]
    // </editor-fold> 
    public void setName (String val) {
        this.Name = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.554DAC30-7972-7201-BC9B-07D3DC509F22]
    // </editor-fold> 
    public int getProteins () {
        return Proteins;
    }


    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.03D99A4D-9527-AA74-A554-542B82933639]
    // </editor-fold> 
    public void setProteins (int val) {
        this.Proteins = val;
    }

 }

