package Entities;

import com.eaio.uuid.UUID;
import java.util.Calendar;
import java.util.Date;


// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.4BB1E480-37B2-7E1B-0DDF-CCAFD9154702]
// </editor-fold> 
public class RecordInDairy {

    private UUID id;

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public UUID getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(UUID id) {
        this.id = id;
    }

    private Products product;

    
    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }
    private Exercise exercise;
            
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.48D99F8F-D635-4AED-7801-A54FD8B51E87]
    // </editor-fold> 
    private Date DateAndTIme;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.6EF3DCE0-5DED-9156-1378-9A919998E823]
    // </editor-fold> 
    private double CurWeight;

   
    public static enum typeOfRec{
         PRODUCT, EXERCISE, WEIGHT
    }
   public static String[] typesToString(){
       return new String[]{"Продукт\\Напиток","Упражнение","Вес"};
   }
    private typeOfRec type;

    public typeOfRec getType() {
        return type;
    }

    public void setType(typeOfRec type) {
        this.type = type;
    }
    
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.8E7EC2B6-134B-EA41-EB2C-3BCE3A897AA3]
    // </editor-fold> 
    /**
     * Количество продукта(N* 100 грамм), напитка (N*100 мл)  или длительность упражнения (N*час)
     */
    private double NumOfProductOrExercise;

    
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.0E4B045A-1FC8-5DD8-3C0D-EE2600063A1A]
    // </editor-fold> 
    /**
     * Каждая запись - это ВРЕМЯ и только ОДИН продукт\напиток или физ. нагрузка/
     * Может(!но не обяз) также содержать информацию о текущем весе.
     * По умолчанию вес=-1, количество продукта\нагрузки = -1.
     * @param timeofRecord 
     * @param typeOfRec - RecordInDairy.typeOfRec._
     */
    public RecordInDairy (Date timeofRecord, typeOfRec type) {
        // default settings
        this.CurWeight = -1;
        this.NumOfProductOrExercise = 0 ;
        
        this.DateAndTIme = timeofRecord;
        this.type = type;
        if(type!=null){
            switch (type) {
                case PRODUCT: this.product = new Products();
                case EXERCISE: this.exercise = new Exercise();
                case WEIGHT: this.CurWeight = 0;
                default:
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.7E21354C-AD70-C4A6-2635-AD53DE0F2961]
    // </editor-fold> 
    public double getCurWeight () {
        return CurWeight;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.E7190EE7-B6F5-5DD0-2EEC-122C99A76C27]
    // </editor-fold> 
    public void setCurWeight (double val) {
        this.CurWeight = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.85BD6114-99C5-9043-A088-7DB5A26B81CB]
    // </editor-fold> 
    public Date getDateAndTIme () {
        return DateAndTIme;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.A258838B-09FE-5ACB-15AE-0A481B47B3EB]
    // </editor-fold> 
    public void setDateAndTIme (Date val) {
        this.DateAndTIme = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.7443D568-5E8C-AA62-78D4-8EACE2D64C59]
    // </editor-fold> 
    
    public double getNumOfProductOrExercise () {
        return NumOfProductOrExercise;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.9104F125-DF73-45B0-8E73-1714FA35E773]
    // </editor-fold> 
    public void setNumOfProductOrExercise (double val) {
        this.NumOfProductOrExercise = val;
    }

    public Date getOnlyDate(){
        // собираем все в одну дату
        Calendar calendar=Calendar.getInstance();
        Date realdate= new Date(this.DateAndTIme.getTime());
        calendar.setTime(realdate);
        //устаналиваем всё в одну дату
        calendar.set(Calendar.HOUR_OF_DAY, 0 );
        calendar.set(Calendar.MINUTE, 0 );
        calendar.set(Calendar.SECOND, 0 );        
        return calendar.getTime();
    }
}

