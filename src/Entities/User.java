package Entities;

import DataLayer.UserGateway;
import com.eaio.uuid.UUID;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.oval.constraint.*;

public class User {
//    
//    //  подумать как сделать Нормально хранение этих переменных
//    private static final String[] IMTdescription= {"выраженный дефицит массы","недостаточная масса",
//        "нормальный вес","избыточная масса","ожирение 1 степени","ожирение 2 степени","ожирение 3 степени"};
// 
    /**
     *  Уникальный идентификатор пользователя
     */
    private UUID id;

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public UUID getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(UUID id) {
        this.id = id;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.2C92ADE9-5F74-1B06-C833-AB664E1812B8]
    // </editor-fold> 
    @NotNull
    private String login;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.7D86E194-2314-1E10-CD6C-38B89A155BB8]
    // </editor-fold> 
    @NotNull
    private String password;

    @NotNull
    private boolean isAdmin = false;

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
    
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.9024F462-D2C7-D638-4D86-FF1A26C2E66F]
    // </editor-fold> 
    // = true - mean it is a men
    // = false - mean it is a girl
    @NotNull
    private boolean male;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.2573ABEF-A9EC-A8CE-BB5E-32346AD61076]
    // </editor-fold> 
    @NotNull
    @Range(min = 18,max = 100)
    private int age;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.0BE508F2-82DC-98DD-0689-0D5AFE74446F]
    // </editor-fold> 
    @NotNull
    @Range(min = 100,max = 250)
    private int height;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.B5EDD29A-E872-288D-4513-98F6EF91D175]
    // </editor-fold> 
    @NotNull
    @Range(min = 20,max = 250)
    private int weight;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.F4CBFF97-75E7-A57D-6E38-3110C1B68EFE]
    // </editor-fold> 
    @NotNull
    private int ExecriseLevel;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.0B55AB7B-4ED5-27C6-3DE2-D6022467DA60]
    // </editor-fold> 
    /**
     * Create NEW User object. If 'login' exist in DB - throw Exception
     * @param login
     * @param pass 
     */
    public User (@NotNull String login, @NotNull String pass) {        
        this.login = login;
        this.password = pass;
       
        // default settings of new User object
        this.ExecriseLevel = 0;
        this.age = 18;
        this.height = 164;
        this.isAdmin = false;
        this.male = false;
        this.weight = 56;
//        this.userDiary = new Diary();
    }
    
    /**
     * Create new User object and add it do DB
     */
   /*
    public void createNewUser(@NotNull String login, @NotNull String pass){
        try {
            // создаем объект
            User usr = new User(login, pass);
            // добавляем его в БД
            UserGateway usrmap = new UserGateway();
            usrmap.insertNewUsr(usr);
        } catch (Exception ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    */
      /**
     * Set basic information about User
     * @param male - true if it is Men, overwise - false
     * @param age
     * @param height
     * @param weight
     * @param exLevel - can be 0, 1, 2, 3 
     */
    public void setDataAboutUser(@NotNull boolean male, @NotNegative int age, @NotNegative int height, 
            @NotNegative int weight, @NotNull int exLevel)
    {
        this.age = age;
        this.male = male;
        this.height = height;
        this.weight = weight;
        this.ExecriseLevel = exLevel;
    }
    
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.8F4A4157-9BB9-A2EC-FE5D-21888C58265F]
    // </editor-fold> 
    public int getExerсiseLevel () {
        return ExecriseLevel;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.2CB32A3F-9E3C-F8E8-B0CA-EB94B05E4396]
    // </editor-fold> 
    /**
     * 
     * @param val - 0 if it isn't exercise
     * 1 - if 1-3 exercise per week
     * 2 - if 3-5 exercise per week
     * 3 - if 6-7 exercise per week
     */
    public void setExecriseLevel (@NotNegative int val) {
        this.ExecriseLevel = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.609913C1-5D55-2DBF-D2FE-E359F0AD11B8]
    // </editor-fold> 
    public int getAge () {
        return age;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.866FCC99-61B1-43C1-1328-07860396C377]
    // </editor-fold> 
    public void setAge (@NotNegative int val) {
        this.age = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.F8465197-22C9-00F1-ACE4-B2B33705BD50]
    // </editor-fold> 
    public int getHeight () {
        return height;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.78C361BC-4CE3-F172-599D-06F645D9EF9D]
    // </editor-fold> 
    public void setHeight (@NotNegative int val) {
        this.height = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.9C4ACDA3-ACA7-A265-07C4-FC1F41496FED]
    // </editor-fold> 
    public String getLogin () {
        return login;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.AEA5126E-78A8-02F3-31C6-EDBECC30175C]
    // </editor-fold> 
    public void setLogin (@NotNull String val) {
        this.login = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.87C75C30-2DA7-55F5-962C-EEBB5315D164]
    // </editor-fold> 
    public boolean getMale () {
        return male;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.B4C6E689-92EE-8775-A4A4-DB2F6BCCA5B2]
    // </editor-fold> 
    public void setMale (@NotNull boolean val) {
        this.male = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.78DB008C-DB35-A383-50D7-4DA9BCBA9408]
    // </editor-fold> 
    public String getPassword () {
        return password;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.3E96176A-BFDE-A89C-FC0C-5A6151A2EB83]
    // </editor-fold> 
    public void setPassword (@NotNull String val) {
        this.password = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.B527E8A0-E058-5CF1-6E4A-D2461DC4D028]
    // </editor-fold> 
    public int getWeight () {
        return weight;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.212673B2-51A4-E5CB-C31F-CA1BBD01C7A4]
    // </editor-fold> 
    public void setWeight (@NotNegative int val) {
        this.weight = val;
    }

//    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
//    // #[regen=yes,id=DCE.E7C706E7-8242-678A-C75D-80538EE0610F]
//    // </editor-fold> 
//    @NotNegative
//    @NotNull
//    public double CalcIMT () {           
//        return (double)10000* weight/(height*height);
//    }
//
//    /**
//     * Check IMT. Use standart table values for IMT and check are weight of User normal
//     * @param IMT - type=int. Value of IMT.
//     * @return 
//     */
//    public String checkIMT(int IMT){
//        if (IMT<16) return this.IMTdescription[0];
//        else if (16<=IMT && IMT<=17.9) return this.IMTdescription[1];
//        else if (18<=IMT && IMT<=24.9) return this.IMTdescription[2];
//        else if (25<=IMT && IMT<=29.9) return this.IMTdescription[3];
//        else if (30<=IMT && IMT<=34.9) return this.IMTdescription[4];
//        else if (35<=IMT && IMT<=39.9) return this.IMTdescription[5];
//        else if(IMT>=40) return this.IMTdescription[6];
//        else return null;
//    }
//    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
//    // #[regen=yes,id=DCE.37E0C6D6-94BD-E4CD-A8E9-BEBDA4DB572E]
//    // </editor-fold> 
//    /**
//     * Calculate the basic level of Kkal per day
//     * @return 
//     */
//    public double CalcKkalPerDay () {
//        double defaultKkal = 0;
//        if (this.male == true) {
//         defaultKkal = 66.47 + 13.75 * this.weight + 5 * this.height - 6.74 * this.age;
//        } else if(this.male == false){
//           defaultKkal = 655.1 + 9.6 * this.weight + 1.85 * this.height - 4.68 * this.age;
//        }
//        double exCoef = 1;
//        switch(this.ExecriseLevel){
//            case 0: exCoef = 1.2; break;
//            case 1: exCoef = 1.375; break;
//            case 2: exCoef = 1.4625; break;
//            case 3: exCoef = 1.6375; break;
//            default:                 
//         }
//        return exCoef*defaultKkal;
//    }
//    
//    /**
//     * Функция проверки того, сколько пользователь "наел" за текущий день
//     * Если не превысило количество ккал в этот день - то просто возвращает 
//     * эту величину ("Ваша норма калорий на день: __ "). 
//     * Если превышает - то возвращается надпись "Ваша норма калорий на день ( __ ) превышена на __ !!! "
//     * @param kkal
//     * @return 
//     */
//    public Map<Integer,Object> checkKkalInThisDay(double kkal){
//        String result = null;
//        double kkalDef = this.CalcKkalPerDay();
//        if(kkalDef==0) // какие-то параметры у пользователя не введены 
//            return null;
//        // если у пользователя указаны все параметры, то рассчитываем их
//        result="Ваша норма калорий на день: "+kkalDef;
//        Map<Integer,Object> res = new HashMap<>();
//        
//        if(kkal <= kkalDef)  res.put(1,true);
//        else { 
//            result+=" превышена на "+(kkal-kkalDef)+" !!!";
//            res.put(1, false);
//        }
//        res.put(0,result);
//        return res;
//    }
//    
//    
//    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
//    // #[regen=yes,id=DCE.68457668-4E54-4EDF-CA8A-C17817809317]
//    // </editor-fold> 
//    /**
//     * 
//     * @param type :
//     * 0 - 'normal' scheme 30 20 50
//     * 1 - 'low proteins' scheme 15 20 65
//     * 2 - 'low fat' scheme 40 15 45
//     * 3 - 'low carb' scheme 65 20 15
//     */
//    public String CalcPFC (int type) {
//       // Kkal in 1 gramm :
//        int fat = 9;
//        int protein = 4;
//        int carb = 4;
//        // calculate normal Kkal
//        double defKkalPerDay = this.CalcKkalPerDay();
//        // calculate Low and High Kkal level 
//        double LowKkal = defKkalPerDay - 250;
//        double HeightKkal = defKkalPerDay + 100;
//        // type of scheme PFC
//        double[][] PFCtype = {  {0.3, 0.20, 0.50},   /*normal*/
//                                {0.15, 0.20, 0.65},   /*low prot*/
//                                {0.40, 0.15, 0.45},   /*low fat*/
//                                {0.65, 0.20, 0.15}    /*low carb*/
//                            };               
////        // TODO : think about type 'Map<String,Double>' - maybe another format use ?
////        Map<String, Double> kal = new HashMap<String, Double>();
////        kal.put("proteinLow", LowKkal*PFCtype[type][0]/protein); 
////        kal.put("proteinHigh", HeightKkal*PFCtype[type][0]/protein); 
////        kal.put("fatLow", LowKkal*PFCtype[type][1]/fat); 
////        kal.put("fatHigh", HeightKkal*PFCtype[type][1]/fat); 
////        kal.put("carbLow", LowKkal*PFCtype[type][2]/carb); 
////        kal.put("carbHigh", HeightKkal*PFCtype[type][2]/carb); 
//        
//        String result = "";
//        result+="Белки: "+ String.valueOf(LowKkal*PFCtype[type][0]/protein)+"-"+String.valueOf(HeightKkal*PFCtype[type][0]/protein)+"\n";
//        result+="Жиры: "+ String.valueOf(LowKkal*PFCtype[type][1]/fat)+"-"+String.valueOf(HeightKkal*PFCtype[type][1]/fat)+"\n";
//        result+="Углеводы: "+ String.valueOf(LowKkal*PFCtype[type][2]/carb)+"-"+String.valueOf(HeightKkal*PFCtype[type][2]/carb)+"\n";
//        // возвращаем строку из названия переменной и ее значения
//        return result;
//    }

}

