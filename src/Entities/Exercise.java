package Entities;

import com.eaio.uuid.UUID;
import net.sf.oval.constraint.NotNull;


// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.F998F50E-BA87-5EAB-6A43-7F9A339C6AEE]
// </editor-fold> 
public class Exercise {

    //@NotNull
    private UUID id;

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public UUID getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(UUID id) {
        this.id = id;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.62D19E35-A3E0-1FB9-7A17-C7E6437A3703]
    // </editor-fold> 
    private String Name;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.D7634F4A-755C-CBDD-6EEB-D7A7FC97144F]
    // </editor-fold> 
    private int KkalPerHour;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.3F5D1BE3-EF39-8243-967C-7E40BA38CE08]
    // </editor-fold> 
    public Exercise (String Name, int KkalPerHour) {
        this.KkalPerHour = KkalPerHour;
        this.Name = Name;               
    }
    /**
     * Default constructor. Name = 'name', KkalPerHour = 0
     */
    public Exercise () {
        this.KkalPerHour = 0 ;
        this.Name = "name";               
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.16EAFC9D-FBA0-0E99-00E7-3767A4368BB3]
    // </editor-fold> 
    public int getKkalPerHour () {
        return KkalPerHour;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.A898DCCB-20B2-A3F6-FCF1-BCEAD75C9378]
    // </editor-fold> 
    public void setKkalPerHour (int val) {
        this.KkalPerHour = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.AEB007A5-4DCF-E9B3-3F8E-3F77556560C0]
    // </editor-fold> 
    public String getName () {
        return Name;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.527125C5-5D02-0875-400B-40C0F1E79236]
    // </editor-fold> 
    public void setName (String val) {
        this.Name = val;
    }

}

