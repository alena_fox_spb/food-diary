/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swingGUI;

//import BusinessLogic.Exercise;
//import BusinessLogic.Products;
//import Entities.RecordInDairy;
//import ServiceLayer.NamesOfProdExService;
//import ServiceLayer.RecordsService;
import RemoteSystem.RMIClient;
import RemoteSystem.ServiceInterface;
import com.eaio.uuid.UUID;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author Alena_Fox
 */
public class AddRecordFrame extends javax.swing.JFrame {

   //Замена типов "RecordInDairy.typeOfRec":
    public static enum typeOfRec{
         PRODUCT, EXERCISE, WEIGHT
    }
    private String[] typesOfField = {"Продукт\\Напиток","Упражнение","Вес"};
    // для отображения окна даты
    final JFrame dateFrame = new JFrame();
    // для отображения значений из комбо бокса 
    DefaultComboBoxModel comboData = new DefaultComboBoxModel();
    // для работы с сервисным слоем Записей в дневнике
    //RecordsService service = new RecordsService();
    // объект, через который посылаются удаленные методы и получаются овтеты соотв-но 
     ServiceInterface serverObject = RMIClient.GetInstanceServerObject();
    // UUID user
    UUID usrId;
    // чтобы обновлять данные в текущем окно сразу при добавлении новой записи-
    public RecordFrame record = RecordFrame.getFrame();
    
    /**
     * Creates new form addRecord
     */
    public AddRecordFrame(UUID id) {
        usrId = id;
        initComponents();
        try {
            // чтобы установить значения в комбо бокс. По умолчанию отображаются продукты:
            //setComboBoxData(NamesOfProdExService.getProductNames());
            setComboBoxData(serverObject.getProductNames());
        } catch (RemoteException ex) {
            Logger.getLogger(AddRecordFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setComboBoxData(String[] data){
        comboData.removeAllElements();
        for( String newRow : data ) {
            comboData.addElement( newRow );
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jSpinner1 = new javax.swing.JSpinner(new SpinnerDateModel());
        jTextField1 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox();
        jComboBoxName = new javax.swing.JComboBox();
        jTextField2 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Добавить запись");

        jLabel1.setText("Дата:");

        jLabel2.setText("Время:");

        jLabel3.setText("Тип записи:");

        jLabel4.setText("Название");

        jLabel5.setText("Количество");

        jLabel6.setText("(Продукты: г; Напитки: мл; Физ. нагрузка: часов; Вес: кг)");
        jLabel6.setEnabled(false);

        jButton1.setText("Ok");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextField1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField1MouseClicked(evt);
            }
        });
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(typesOfField));
        jComboBox1.setSelectedItem(null);
        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });

        jComboBoxName.setModel(comboData);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(20, 20, 20))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSpinner1)
                            .addComponent(jTextField1)
                            .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBoxName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jComboBoxName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap())
        );

        JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(jSpinner1, "HH:mm:ss");
        jSpinner1.setEditor(timeEditor);
        jSpinner1.setValue(new Date()); // will only show the current time

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Добавляем запись в дневник
     * @param evt 
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // получаем дату
        String date = jTextField1.getText();
        System.out.println("date:"+date);
        if(date==null || date.equals("")){
            JOptionPane.showMessageDialog(null, "Введите дату!");
        }
        // получаем время из спиннера
        Date da = (Date)jSpinner1.getValue();
        Date realDate = collectDates(date,da); // вызываем в GUI, тк эта дата нам нужна при отображении нового окна
        String selectedItemName = ""; // название

        // получаем тип записи
        Object ret = jComboBox1.getSelectedItem();          
        int n = jComboBoxName.getSelectedIndex();
        selectedItemName = (String)jComboBoxName.getItemAt(n);
        // получаем колво
        String num = jTextField2.getText();
        checkParams(num);
        try {
            // говорим сервису. чтобы обработал и привел данные к нужному виду
            // и  отдал их мапперу для добавления в БД
            //service.addNewRecord(realDate, usrId, ret, selectedItemName, num);
            serverObject.addNewRecord(realDate, usrId, ret, selectedItemName, num);
        } catch (RemoteException ex) {
            Logger.getLogger(AddRecordFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // обновляем данные в текущем окне RecordFrame / Тип Date дб : yyyy-MM-dd
        record.createRecordFrame(usrId, new java.sql.Date(realDate.getTime()));
        record.setVisible(true);
        // скрываем окно добавления записи
        this.setVisible(false);
        
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * В данном окне две формы для ввода даты и времени - этот метод 
     * собирает эти два параметра в один объект Date
     * @param date1 - дата в формате dd-MM-yyyy (String)
     * @param date2 - время в формате Date
     * @return 
     */
    private Date collectDates(String date1, Date date2){
         // дата 
        Date dates= null;
        try {
            dates = new SimpleDateFormat("dd-MM-yyyy").parse(date1);
        } catch (ParseException ex) {
            Logger.getLogger(AddRecordFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        // собираем все в одну дату
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date2);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        int sec = calendar.get(Calendar.SECOND);
        // теперь устаналиваем всё в одну дату - dates
        calendar.setTime(dates);
        calendar.set(Calendar.HOUR_OF_DAY,hour );
        calendar.set(Calendar.MINUTE, min );
        calendar.set(Calendar.SECOND, sec );
        
        Date realdate=calendar.getTime();
        return realdate;
    }
    
    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jTextField1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField1MouseClicked
        // TODO add your handling code here:
        jTextField1.setText(new DatePicker(dateFrame).setPickedDate());        
        
    }//GEN-LAST:event_jTextField1MouseClicked

    /**
     * Меняем тип записи
     * @param evt 
     */
    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        takeTypeOfRec();
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private Object takeTypeOfRec(){
        String [] items=new String[]{""};
        // по умолчанию
        Object ret = typeOfRec.PRODUCT;;
        // получаем значение из комбо бокса с типом записи
        Object selectedItem = jComboBox1.getSelectedItem();
        try {
            // проверяем тип
            if(selectedItem.toString().equals("Продукт\\Напиток")) {            
                    //items = NamesOfProdExService.getProductNames();
                    items = serverObject.getProductNames();
                    ret = typeOfRec.PRODUCT;            
            }
            else if(selectedItem.toString().equals("Упражнение"))   {
                        //items = NamesOfProdExService.getExcercisesNames();
                        items = serverObject.getExcercisesNames();
                        ret = typeOfRec.EXERCISE;
                    }    
            else if(selectedItem.toString().equals("Вес")){
                items=new String[]{"Укажите вес в поле 'Кол-во'"};
                ret = typeOfRec.WEIGHT;
            }
        } catch (RemoteException ex) {
                Logger.getLogger(AddRecordFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        // выводим сответствующие данные в комбо бокс следующий
        setComboBoxData(items);
        return ret;
    }
    
    // для поля ввода "количества"
    protected MaskFormatter createFormatter(String s) {
        MaskFormatter formatter = null;
        try {
            formatter = new MaskFormatter(s);
        } catch (java.text.ParseException exc) {
            System.err.println("formatter is bad: " + exc.getMessage());
            System.exit(-1);
        }
        return formatter;
}


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBoxName;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables

    /**
     * Проверяем - что введенные параметры - это числа и уведомляем если нет
     * @param num 
     */
    private void checkParams(String num) {
       double count = 0;
        try{
            // проверяем - что введено число
            if(num!=null && !num.equals("")) count = Double.parseDouble(num);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (count<0) throw new NumberFormatException();
        } catch(NumberFormatException e){
            JOptionPane.showMessageDialog(null, "Введите число больше нуля!");
        }
    }

    // класс для отображения окна выбора даты
 public  class DatePicker {
        int month = java.util.Calendar.getInstance().get(java.util.Calendar.MONTH);
        int year = java.util.Calendar.getInstance().get(java.util.Calendar.YEAR);;
        JLabel l = new JLabel("", JLabel.CENTER);
        String day = "";
        JDialog d;
        JButton[] button = new JButton[49];

        public DatePicker(JFrame parent) {
                d = new JDialog();
                d.setModal(true);
                String[] header = { "Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat" };
                JPanel p1 = new JPanel(new GridLayout(7, 7));
                p1.setPreferredSize(new Dimension(430, 120));

                for (int x = 0; x < button.length; x++) {
                        final int selection = x;
                        button[x] = new JButton();
                        button[x].setFocusPainted(false);
                        button[x].setBackground(Color.white);
                        if (x > 6)
                                button[x].addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent ae) {
                                                day = button[selection].getActionCommand();
                                                d.dispose();
                                        }                            
                                });
                        if (x < 7) {
                                button[x].setText(header[x]);
                                button[x].setForeground(Color.red);
                        }
                        p1.add(button[x]);
                }
                JPanel p2 = new JPanel(new GridLayout(1, 3));
                JButton previous = new JButton("<< Previous");
                previous.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                                month--;
                                displayDate();
                        }
                });
                p2.add(previous);
                p2.add(l);
                JButton next = new JButton("Next >>");
                next.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                                month++;
                                displayDate();
                        }
                });
                p2.add(next);
                d.add(p1, BorderLayout.CENTER);
                d.add(p2, BorderLayout.SOUTH);
                d.pack();
                d.setLocationRelativeTo(parent);
                displayDate();
                d.setVisible(true);
        }

        public void displayDate() {
                for (int x = 7; x < button.length; x++)
                        button[x].setText("");
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
                                "MMMM yyyy");
                java.util.Calendar cal = java.util.Calendar.getInstance();
                cal.set(year, month, 1);
                int dayOfWeek = cal.get(java.util.Calendar.DAY_OF_WEEK);
                int daysInMonth = cal.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);
                for (int x = 6 + dayOfWeek, day = 1; day <= daysInMonth; x++, day++)
                        button[x].setText("" + day);
                l.setText(sdf.format(cal.getTime()));
                d.setTitle("Date Picker");
        }

        public String setPickedDate() {
                if (day.equals(""))
                        return day;
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
                                "dd-MM-yyyy");
                java.util.Calendar cal = java.util.Calendar.getInstance();
                cal.set(year, month, Integer.parseInt(day));
                return sdf.format(cal.getTime());
        }
}
    
}
