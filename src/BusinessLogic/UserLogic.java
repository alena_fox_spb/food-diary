package BusinessLogic;

import DataLayer.UserGateway;
import Entities.User;
import ServiceLayer.UserService;

import com.eaio.uuid.UUID;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.oval.constraint.NotNegative;
import net.sf.oval.constraint.NotNull;

/**
 * Бизнес-логика. Методы в стиле "сценарий транзакции" - оперируем пакетами Сущностей 
 * и Слоем доступа к данным, при этом используем специфичную для каждого действия ("транзакции")
 * логику = бизнес-логика.
 * 
 * @author Alena_Fox
 */
public class UserLogic {
    
    
    //  подумать как сделать Нормально хранение этих переменных
    private static final String[] IMTdescription= {"выраженный дефицит массы","недостаточная масса",
        "нормальный вес","избыточная масса","ожирение 1 степени","ожирение 2 степени","ожирение 3 степени"};
 
    /**
     * Отправляем запрос мапперу о создании нового пользователя
     * @param login
     * @param pass 
     * @return id нового пользователя
     */
    public static UUID createUser(String login, String pass){
        if(login.equals("")||pass.equals("")) return null;
        
        UserGateway usrmap = new UserGateway();
        try {
            User usr = new User(login, pass);
            usrmap.insertNewUsr(usr);
            return usr.getId();
        } catch (Exception ex) {
            Logger.getLogger(UserLogic.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
       
    /**
     * По получаемым параметрам с помощью бизнес-логикик 
     * считает БЖУ и возращает его в формате String
     * @param isMale
     * @param age
     * @param exLevel
     * @param type
     * @param height
     * @param weight
     * @return 
     */
    public static String calcBZU(int isMale, int age, int exLevel, int type,
            int height, int weight){
        boolean male=false; 
        switch(isMale){
            case 0 : male = false; break; // женщина
            case 1: male = true; break;
        }
        // расчет калорийности
        try{
            User usr = new User("", "");
            usr.setAge(age);
            usr.setExecriseLevel(exLevel);
            usr.setHeight(height);
            usr.setWeight(weight);
            usr.setMale(male);
            return String.valueOf(UserLogic.CalcPFC(usr,type));
        }catch(Exception e){
                    }
        return "";
    }
    
    public  static String[] calcIMT(int height, int weight){
       String[] resString = new String[2];
        // расчет ИМТ
        try{
            User usr = new User(" ", " ");
            usr.setHeight(height);
            usr.setWeight(weight);
            double CalcIMT = UserLogic.CalcIMT(usr);
            resString[0] = String.valueOf(CalcIMT);
            resString[1] = UserLogic.checkIMT((int)CalcIMT);
            return resString;
        }catch(Exception e)    {
        }
        return null;
    }
    /**
     * Рассчитывается калорийность и обновляются данные о пользователе (через маппер)
     * @param isMale
     * @param age
     * @param exLevel
     * @param height
     * @param weight
     * @return 
     */
    public  static String calcKkal(UUID usrid, int isMale, int age, int exLevel,
            int height, int weight){
        if(usrid==null) return null;
        boolean male=false; 
        switch(isMale){
            case 0 : male = false; break; // женщина
            case 1: male = true; break;
        }
        // расчет калорийности
        try{
            // вызваем маппер для обновления данных о пользователе
            UserGateway mapper = new UserGateway();            
            User usr = mapper.findbyID(usrid);
            usr.setAge(age);
            usr.setExecriseLevel(exLevel);
            usr.setHeight(height);
            usr.setWeight(weight);
            usr.setMale(male);
            // обновляем инфу о пользователе
            mapper.updateUsrInfo(usr);
            
            return String.valueOf(UserLogic.CalcKkalPerDay(usr));
        }catch(Exception e){
                    }
        return "";
    }
    /**
     * Обращается к бизнес-логике для определения - переел ли человек сегодня или нет
     * Возвращает мапу - первый объект которой - строка с результатом,
     * второй объект - превышен лимит или нет
     * @param usr
     * @return 
     */
    public  static Map<Integer,Object> checkKkal(UUID usrId, Double kkal){
        if(usrId==null) return null;
        Map<Integer,Object> res = new HashMap<>();
        // по  умолчанию при создании объекта User у него указываются какие-то
        // значения пола, веса, возраста и тд.       
        UserGateway mapper = new UserGateway();
        try {
            // получаем данные о пользователе из бд
            User user = mapper.findbyID(usrId);
            // выполняем вычисления сами непосредственно
            res = UserLogic.checkKkalInThisDay(user, kkal);            
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    // ----------------------------------------------------------
    // вспомогательные методы, занимающиеся конкретными подсчетами
    @NotNegative
    @NotNull
    public static double CalcIMT (User usr) {           
        return (double)10000* usr.getWeight()/(usr.getHeight()*usr.getHeight());
    }

    /**
     * Check IMT. Use standart table values for IMT and check are weight of User normal
     * @param IMT - type=int. Value of IMT.
     * @return 
     */
    public static String checkIMT(int IMT){
        if (IMT<16) return IMTdescription[0];
        else if (16<=IMT && IMT<=17.9) return IMTdescription[1];
        else if (18<=IMT && IMT<=24.9) return IMTdescription[2];
        else if (25<=IMT && IMT<=29.9) return IMTdescription[3];
        else if (30<=IMT && IMT<=34.9) return IMTdescription[4];
        else if (35<=IMT && IMT<=39.9) return IMTdescription[5];
        else if(IMT>=40) return IMTdescription[6];
        else return null;
    }
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.37E0C6D6-94BD-E4CD-A8E9-BEBDA4DB572E]
    // </editor-fold> 
    /**
     * Calculate the basic level of Kkal per day
     * @return 
     */
    public static double CalcKkalPerDay (User usr) {
        double defaultKkal = 0;
        if (usr.getMale() == true) {
         defaultKkal = 66.47 + 13.75 * usr.getWeight() + 5 * usr.getHeight() - 6.74 * usr.getAge();
        } else if(usr.getMale() == false){
           defaultKkal = 655.1 + 9.6 * usr.getWeight() + 1.85 * usr.getHeight() - 4.68 * usr.getAge();
        }
        double exCoef = 1;
        switch(usr.getExerсiseLevel()){
            case 0: exCoef = 1.2; break;
            case 1: exCoef = 1.375; break;
            case 2: exCoef = 1.4625; break;
            case 3: exCoef = 1.6375; break;
            default:                 
         }
        return exCoef*defaultKkal;
    }
    
    /**
     * Функция проверки того, сколько пользователь "наел" за текущий день.
     * Возрващается Map из 2 объектов:
     * Первый объект - строка с результатом: <0, String> 
     * Второй объект - это boolean - говорящий, превышена ккал или нет: <1,true\false> 
     * 
     * Если не превысило количество ккал в этот день - то просто возвращает 
     * эту величину ("Ваша норма калорий на день: __ "). 
     * Если превышает - то возвращается надпись "Ваша норма калорий на день ( __ ) превышена на __ !!! "
     * @param kkal
     * @return 
     */
    public static Map<Integer,Object> checkKkalInThisDay(User usr, double kkal){
        String result = null;
        double kkalDef = UserLogic.CalcKkalPerDay(usr);
        if(kkalDef==0) // какие-то параметры у пользователя не введены 
            return null;
        // если у пользователя указаны все параметры, то рассчитываем их
        result="Ваша норма калорий на день: "+kkalDef;
        Map<Integer,Object> res = new HashMap<>();
        
        if(kkal <= kkalDef)  res.put(1,true);
        else { 
            result+=" превышена на "+(kkal-kkalDef)+" !!!";
            res.put(1, false);
        }
        res.put(0,result);
        return res;
    }
    
    
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.68457668-4E54-4EDF-CA8A-C17817809317]
    // </editor-fold> 
    /**
     * 
     * @param type :
     * 0 - 'normal' scheme 30 20 50
     * 1 - 'low proteins' scheme 15 20 65
     * 2 - 'low fat' scheme 40 15 45
     * 3 - 'low carb' scheme 65 20 15
     */
    public  static String CalcPFC (User usr, int type) {
       // Kkal in 1 gramm :
        int fat = 9;
        int protein = 4;
        int carb = 4;
        // calculate normal Kkal
        double defKkalPerDay = UserLogic.CalcKkalPerDay(usr);
        // calculate Low and High Kkal level 
        double LowKkal = defKkalPerDay - 250;
        double HeightKkal = defKkalPerDay + 100;
        // type of scheme PFC
        double[][] PFCtype = {  {0.3, 0.20, 0.50},   /*normal*/
                                {0.15, 0.20, 0.65},   /*low prot*/
                                {0.40, 0.15, 0.45},   /*low fat*/
                                {0.65, 0.20, 0.15}    /*low carb*/
                            };               
        String result = "";
        result+="Белки: "+ String.valueOf(LowKkal*PFCtype[type][0]/protein)+"-"+String.valueOf(HeightKkal*PFCtype[type][0]/protein)+"\n";
        result+="Жиры: "+ String.valueOf(LowKkal*PFCtype[type][1]/fat)+"-"+String.valueOf(HeightKkal*PFCtype[type][1]/fat)+"\n";
        result+="Углеводы: "+ String.valueOf(LowKkal*PFCtype[type][2]/carb)+"-"+String.valueOf(HeightKkal*PFCtype[type][2]/carb)+"\n";
        // возвращаем строку из названия переменной и ее значения
        return result;
    }
}
