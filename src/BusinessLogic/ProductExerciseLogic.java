/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BusinessLogic;

import DataLayer.ProductsGateway;
import Entities.Products;
import ServiceLayer.UploadJsonFileToDB;

/**
 *
 * @author Alena_Fox
 */
public class ProductExerciseLogic {
    public static void addNewProduct(String name, Boolean isProduct, int kkal, 
            int prot,int fat,int carb){
      
        // перед тем как положить - всегда считываем из файла данные
        UploadJsonFileToDB.readJsonFile("D:\\products.json");
        
        // засовываем в новый объект Products
        Products product = new Products(name, true, kkal, prot, fat, carb);
        
        // пытаемся положить в БД
        // тут проверка - вдруг такой продукт уже есть в БД - тогда обновить его в базе
        ProductsGateway mapper = new ProductsGateway();
        mapper.checkExistProductAndInsertOrUpdateIt(product);
    }
}
