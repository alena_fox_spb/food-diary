package BusinessLogic;

import DataLayer.DiaryRecordGateway;
import DataLayer.ExercisesGateway;
import DataLayer.ProductsGateway;
import Entities.Exercise;
import Entities.Products;
import Entities.RecordInDairy;
import ServiceLayer.RecordsService;

import com.eaio.uuid.UUID;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alena_Fox
 */
public class RecordLogic {
    /** 
     * Запись о превышении нормы ккал за день : Map из 2 объектов - 
     * Первый объект - строка с результатом: <0, String> 
     * Второй объект - это boolean - говорящий, превышена ккал или нет: <1,true\false> 
     */ 
    public static Map<Integer, Object> checkKkal;
        
    /**
     * Метод вызывает в себе маппер, который получает все записи пользователя по
     * его uid.
     *
     * @return массив строк, пригодный для отображения
     */
    public static String[][] getDiary(UUID usrID) {
        if(usrID==null) return null;
        DiaryRecordGateway gateway = new DiaryRecordGateway();
        String[] resDates;
        
            List<RecordInDairy> records = gateway.findbyAllRecordsByUsrID(usrID);
            int len = records.size();

            resDates = new String[len];
            // для записей main frame
            String[][] result = new String[len][3];

            Set<String> dateSet = new HashSet<String>();
            int i = 0, j = 0;
            for (RecordInDairy rec : records) {
                // получаем время записи
                Date dateAndTIme = rec.getOnlyDate();
                java.sql.Date Date = new java.sql.Date(dateAndTIme.getTime());

                if (dateSet.add(Date.toString())) // если запись о новой дате, до добавляем данные в main form
                {
                    if (i > 0) {
                        j++;
                    }
                    result[j][0] = Date.toString();
                    result[j][1] = "";
                    result[j][2] = "";
                }
                // заполняем данные для записей дневника 
                resDates[i] = Date.toString();
                // проверяем тип записи и получаем соответствующую инфу
                if (rec.getType() == RecordInDairy.typeOfRec.EXERCISE) {
                    // если даты совпадают и при этом была запись о физ нагрузке - указываем это для main window
                    if (resDates[i].equals(result[j][0])) {
                        // запись о физ нагрузке
                        result[j][2] = "+";
                    }
                } else if (rec.getType() == RecordInDairy.typeOfRec.WEIGHT) {
                    // если даты совпадают и при этом была запись о весе - указываем это для main window
                    if (resDates[i].equals(result[j][0])) {
                        // запись о физ нагрузке
                        result[j][1] = String.valueOf(rec.getCurWeight());
                    }
                }
                i++;
            }
            return result; // возвращаем данные для main window
        
    }

    /**
     * Метод, предоставляющий информацию из дневника пользователя, пригодную для экспорта во внеш сервис
     * Работает с маппером и включает свою логику в формировании итогового массива строк.
     * @param usrID
     * @return 
     */
    public static String[][] getDiaryToExport(UUID usrID) {
        if(usrID==null) return null;
        DiaryRecordGateway gateway = new DiaryRecordGateway();
        String[][] resultProduct;
           List<RecordInDairy> records = gateway.findbyAllRecordsByUsrID(usrID);
            int len = records.size();
            // для записей типа продукт: время, название, колво, Б,Ж,У, Ккал
            resultProduct = new String[len][7];
            // для записей main frame
            String[][] result = new String[len][3];

            Set<String> dateSet = new HashSet<String>();
            int i = 0, j = 0;
            for (RecordInDairy rec : records) {
                // получаем время записи
                Date dateAndTIme = rec.getOnlyDate();
                java.sql.Date Date = new java.sql.Date(dateAndTIme.getTime());

                if (dateSet.add(Date.toString())) // если запись о новой дате, до добавляем данные в main form
                {
                    if (i > 0) {
                        j++;
                    }
                    result[j][0] = Date.toString();
                    result[j][1] = "";
                    result[j][2] = "";
                }
                // заполняем данные для записей дневника 
                resultProduct[i][0] = Date.toString();
                // проверяем тип записи и получаем соответствующую инфу
                if (rec.getType() == RecordInDairy.typeOfRec.PRODUCT) {
                    Products product = rec.getProduct();
                    // записываем название проудкта
                    resultProduct[i][1] = product.getName();
                    // колво
                    double num = rec.getNumOfProductOrExercise();
                    resultProduct[i][2] = String.valueOf(num);
                    // бжу
                    resultProduct[i][3] = String.valueOf(num * product.getProteins() / 100);
                    resultProduct[i][4] = String.valueOf(num * product.getFats() / 100);
                    resultProduct[i][5] = String.valueOf(num * product.getCarbons() / 100);
                    // ккал 
                    resultProduct[i][6] = String.valueOf(num * product.getKkal() / 100);

                } else if (rec.getType() == RecordInDairy.typeOfRec.EXERCISE) {
                    Exercise exercise = rec.getExercise();
                    // записываем название проудкта
                    resultProduct[i][1] = exercise.getName();
                    // колво
                    double num = rec.getNumOfProductOrExercise();
                    resultProduct[i][2] = String.valueOf(num);
                    // бжу
                    resultProduct[i][3] = "";
                    resultProduct[i][4] = "";
                    resultProduct[i][5] = "";
                    // ккал 
                    resultProduct[i][6] = String.valueOf(-num * exercise.getKkalPerHour());

                } else if (rec.getType() == RecordInDairy.typeOfRec.WEIGHT) {
                    // если даты совпадают и при этом была запись о весе - указываем это для main window
                    if (resultProduct[i][0].equals(result[j][0])) {
                        // запись о физ нагрузке
                        result[j][1] = String.valueOf(rec.getCurWeight());
                    }
                }
                i++;
            }

            return resultProduct; // возвращаем данные для main window
        
    }

    /**
     * Транзакция, в которой получается и формируется выходной массив строк, 
     * хранящий инф-ю о записях конкретного дня. 
     * Параллельно подсчитывается ккал за день - и проверяется, не превышена ли она.
     * @param usrID
     * @param date
     * @return 
     */
    public static String[][] getRecordsByDay(UUID usrID, Date date) {
        if(usrID==null||date==null) return null;
        DiaryRecordGateway gateway = new DiaryRecordGateway();
        String[][] resultProduct;
            // обращаемся к мапперу и получаем данные из БД
            List<RecordInDairy> records = gateway.findRecordsByDate(usrID, date);
            
            int len = records.size() + 1; // "+1" - тк есть еще доп строка кроме записей "ИтогО"
            // для записей типа продукт: время, название, колво, Б,Ж,У, Ккал
            resultProduct = new String[len][7];
            int i = 0, j = 0;
            for (RecordInDairy rec : records) {
                // если запись о весе - то не добавляем ее в таблицу отображения
                if (rec.getType() == RecordInDairy.typeOfRec.WEIGHT) {
                    i++;
                    continue;
                }
                // получаем время записи
                Date dateAndTIme = rec.getDateAndTIme();
                // заполняем данные для записей дневника 
                resultProduct[i][0] = dateAndTIme.toString();
                // проверяем тип записи и получаем соответствующую инфу
                if (rec.getType() == RecordInDairy.typeOfRec.PRODUCT) {
                    Products product = rec.getProduct();
                    // записываем название проудкта
                    resultProduct[i][1] = product.getName();
                    // колво
                    double num = rec.getNumOfProductOrExercise();
                    resultProduct[i][2] = String.valueOf(num);
                    // бжу
                    resultProduct[i][3] = String.valueOf(num * product.getProteins() / 100);
                    resultProduct[i][4] = String.valueOf(num * product.getFats() / 100);
                    resultProduct[i][5] = String.valueOf(num * product.getCarbons() / 100);
                    // ккал 
                    resultProduct[i][6] = String.valueOf(num * product.getKkal() / 100);

                } else if (rec.getType() == RecordInDairy.typeOfRec.EXERCISE) {
                    Exercise exercise = rec.getExercise();
                    // записываем название проудкта
                    resultProduct[i][1] = exercise.getName();
                    // колво
                    double num = rec.getNumOfProductOrExercise();
                    resultProduct[i][2] = String.valueOf(num);
                    // бжу
                    resultProduct[i][3] = "";
                    resultProduct[i][4] = "";
                    resultProduct[i][5] = "";
                    // ккал 
                    resultProduct[i][6] = String.valueOf(-num * exercise.getKkalPerHour());
                }
                i++;
            }
            // последняя строка в любом дневнике конкретного дня - "итого". ее вид:
            // "","Итого","",String.valueOf(prots),String.valueOf(fats),String.valueOf(carbs),String.valueOf(kkals)
            // формируем эту строку
            resultProduct[i][0] = " ";
            resultProduct[i][1] = "";
            resultProduct[i][2] = "Итого";
            // бжу
            double prot = 0, fat = 0, carb = 0, kkal = 0;
            for (String[] row : resultProduct) {
                if (row[3] != null && !row[3].equals("")) {
                    prot += Double.parseDouble(row[3]);
                }
                if (row[4] != null && !row[4].equals("")) {
                    fat += Double.parseDouble(row[4]);
                }
                if (row[5] != null && !row[5].equals("")) {
                    carb += Double.parseDouble(row[5]);
                }
                if (row[6] != null && !row[6].equals("")) {
                    kkal += Double.parseDouble(row[6]);
                }
            }
            resultProduct[i][3] = String.valueOf(prot);
            resultProduct[i][4] = String.valueOf(fat);
            resultProduct[i][5] = String.valueOf(carb);
            // ккал 
            resultProduct[i][6] = String.valueOf(kkal);

            // выполняем транзакцию - подсчет того, переел ли человек или нет (ккал)
            checkKkal = UserLogic.checkKkal(usrID, kkal);
            
            return resultProduct; // возвращаем данные в виде массива String
    }

    /**
     * Добавляем запись в БД. Передаем следующие параметры:
     * дата, id польз-ля, тип записи, кол-во(или вес)\продукт\упражнение 
     * [продукт\упражнение - только с названием, без остальных полей] 
     * @param realDate
     * @param usrId
     * @param typeOfRec
     * @param selName
     * @param numOf 
     */
    public static void addNewRecord(Date realDate, UUID usrId, Object typeOfRec,
            String selName, double numOf) {
        if(usrId==null) return;
        
        DiaryRecordGateway gateway = new DiaryRecordGateway();
        Object takeTypeOfRec = null;
        // проверяем тип
        if (typeOfRec.toString().equals("Продукт\\Напиток")) {
            takeTypeOfRec = RecordInDairy.typeOfRec.PRODUCT;
        } else if (typeOfRec.toString().equals("Упражнение")) {
            takeTypeOfRec = RecordInDairy.typeOfRec.EXERCISE;
        } else if (typeOfRec.toString().equals("Вес")) {
            takeTypeOfRec = RecordInDairy.typeOfRec.WEIGHT;
        }
        // ---
        RecordInDairy rec = null;
        try {
            rec = new RecordInDairy(realDate, (RecordInDairy.typeOfRec) takeTypeOfRec);
            rec.setId(usrId);
            if (takeTypeOfRec == RecordInDairy.typeOfRec.WEIGHT) {
                rec.setCurWeight(numOf);
                rec.setNumOfProductOrExercise(0);
            } else if (takeTypeOfRec == RecordInDairy.typeOfRec.PRODUCT) {
                Products product = new Products();
                product.setName(selName);
                rec.setProduct(product);
                rec.setNumOfProductOrExercise(numOf);
                // получаем id продукта по его названию
                ProductsGateway map = new ProductsGateway();
                Products findByName = map.findByName(rec.getProduct().getName());
                rec.setProduct(findByName);
            } else if (takeTypeOfRec == RecordInDairy.typeOfRec.EXERCISE) {
                Exercise ex = new Exercise();
                ex.setName(selName);
                rec.setExercise(ex);
                rec.setNumOfProductOrExercise(numOf);

                ExercisesGateway map = new ExercisesGateway();
                Exercise findByName = map.findByName(rec.getExercise().getName());
                rec.setExercise(findByName);
            }
            // добавляем запись
            UUID usr = rec.getId();
            rec.setId(null);
            gateway.insert(rec, usr);

        } catch (SQLException ex) {

            Logger.getLogger(RecordsService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
