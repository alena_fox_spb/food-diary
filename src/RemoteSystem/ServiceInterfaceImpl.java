package RemoteSystem;

import ServiceLayer.ServiceFacade;
import com.eaio.uuid.UUID;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author Alena_Fox
 */
public class ServiceInterfaceImpl extends UnicastRemoteObject implements ServiceInterface {
    // фасад, через который мы обращаемся к методам Сервисного слоя
    ServiceFacade facade;
    
    public ServiceInterfaceImpl() throws RemoteException{
        facade = new ServiceFacade();
    }
    
    public void addNewProduct(String name, Boolean isProduct, String kkal, String prot, String fat, String carb) throws RemoteException {
        facade.addNewProduct(name, isProduct, kkal, prot, fat, carb);
    }

   
    public boolean isUserAdmin(UUID currentUsrId) throws RemoteException {
        return facade.isUserAdmin(currentUsrId); 
    }

       
    public UUID createUser(String login, String pass) throws RemoteException {
        return facade.createUser(login, pass);
    }

    
    public boolean checkLogin(String login) throws RemoteException {
        return facade.checkLogin(login);
    }

    
    public UUID checkLoginPass(String login, String pass) throws RemoteException {
        return facade.checkLoginPass(login, pass);
    }

    
    public String[] getProductNames() throws RemoteException {
        return facade.getProductNames();
    }

    
    public String[] getExcercisesNames() throws RemoteException {
        return facade.getExcercisesNames();
    }

   
    public String[][] getDiary(UUID usrID) throws RemoteException {
        return facade.getDiary(usrID);
    }

   
    public Map<Integer, Object> getKkalAtThisDay() throws RemoteException {
        return facade.getKkalAtThisDay();
    }

   
    public String[][] getRecordsByDay(UUID usrID, Object dateVal) throws RemoteException {
        return facade.getRecordsByDay(usrID, dateVal);
    }

   
    public int addNewRecord(Date realDate, UUID usrId, Object typeOfRec, String selName, String num) throws RemoteException {
        return facade.addNewRecord(realDate, usrId, typeOfRec, selName, num);
    }

   
    public String calcBZU(int isMale, String a, int exLevel, int type, String h, String w) throws RemoteException {
        return facade.calcBZU(isMale, a, exLevel, type, h, w);
    }

   
    public String[] calcIMT(String h, String w) throws RemoteException {
        return facade.calcIMT(h, w);
    }

   
    public String calcKkal(UUID usrid, int isMale, String a, int exLevel, String h, String w) throws RemoteException {
        return facade.calcKkal(usrid, isMale, a, exLevel, h, w);
    }

   
    public Map<Integer, Object> checkKkal(UUID usrId, Double kkal) throws RemoteException {
        return facade.checkKkal(usrId, kkal);
    }
    
}
