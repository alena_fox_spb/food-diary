package RemoteSystem;

/**
 *
 * @author Alena_Fox
 */
import java.rmi.*;
import java.rmi.registry.LocateRegistry;

public class RMIServer {
    static final String addr = "//192.168.0.38/remoteObject";
    
    // Registration for RMI serving.
    public static void main(String[] args) {
        // чтобы база не была пустаяпо продуктам
        //ServiceLayer.UploadJsonFileToDB.readJsonFile("D:\\products.json");
        
        try {
            LocateRegistry.createRegistry(1099);
            //Set up security manager for the server part
            //System.setSecurityManager(new RMISecurityManager());
            //Create remote object
            ServiceInterfaceImpl remoteObject = new ServiceInterfaceImpl();
            //Bind remote object into the RMI registry
            Naming.rebind(addr, remoteObject);
            //Echo a message
            System.out.println("Server Ready to work, from address:"+addr);
        } catch (Exception e) { 
            e.printStackTrace(System.err);
        }
    }
}
