package RemoteSystem;

/**
 *
 * @author Alena_Fox
 */
//Import needed packages
import com.eaio.uuid.UUID;
import java.rmi.*;
import java.util.Date;
import java.util.Map;

public interface ServiceInterface extends Remote { 
    
    /**
     * Методы класса-сервиса "AddNewProductService"
     */
    void addNewProduct(String name, Boolean isProduct, String kkal, 
             String prot,  String fat, String carb) throws RemoteException;
    /**
     * Методы класса-сервиса "LoginService"
     */
    boolean isUserAdmin(UUID currentUsrId) throws RemoteException;
    UUID createUser(String login, String pass) throws RemoteException;
    boolean checkLogin(String login) throws RemoteException;
    UUID checkLoginPass(String login, String pass) throws RemoteException;
    /**
     * Методы класса-сервиса "NamesOfProdExService"
     */
    String[] getProductNames() throws RemoteException;
    String[] getExcercisesNames() throws RemoteException;
    /**
     * Методы класса-сервиса "RecordsService"
     */
    String[][] getDiary(UUID usrID) throws RemoteException;
    Map<Integer, Object> getKkalAtThisDay() throws RemoteException;
    String[][] getRecordsByDay(UUID usrID, Object dateVal) throws RemoteException; 
    int addNewRecord(Date realDate, UUID usrId, Object typeOfRec,
            String selName, String num ) throws RemoteException;
    /**
     * Методы класса-сервиса "UserService"
     */
    String calcBZU(int isMale, String a, int exLevel, int type,
            String  h, String  w) throws RemoteException;
    String[] calcIMT(String h, String w) throws RemoteException;
    String calcKkal(UUID usrid, int isMale, String a, int exLevel,
            String h, String w) throws RemoteException;
    Map<Integer,Object> checkKkal(UUID usrId, Double kkal) throws RemoteException;
}
