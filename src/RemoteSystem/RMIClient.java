/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RemoteSystem;

/**
 *
 * @author Alena_Fox
 */
import java.rmi.*;
import java.rmi.registry.*;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RMIClient {

    static final String addr = "//192.168.0.62/remoteObject";
    static ServiceInterface serverObject;
    
    public static ServiceInterface GetInstanceServerObject(){
        if(serverObject==null)  {
            try {
                RMIClient.main(null);
            } catch (Exception ex) {
                Logger.getLogger(RMIClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }  
        return serverObject;
    }
    public static void main(String[] args )throws Exception {        
        try {
//Set security manager for the client application
            //System.setSecurityManager(new RMISecurityManager());
//Lookup for the remote object (treat it as an interface)
            serverObject = (ServiceInterface) Naming.lookup(addr);
//Call the function of the remote object - display client's time and server's time
            //System.out.println("IMT:\n" + Arrays.toString(serverObject.calcIMT("178", "69")));
        } catch(java.rmi.ConnectException e){
                 System.out.println("Не удается подключиться к удаленному серверу для работы");
                 System.exit(0);
             }
        /*(Exception e) {            e.printStackTrace(System.err);           
        }*/
    }
}
