package ServiceLayer;

import BusinessLogic.UserLogic;
import DataLayer.UserGateway;
import com.eaio.uuid.UUID;
import java.io.Serializable;

/**
 * Класс, который берет инфу от форм и использует методы из мапперов.
 * @author Alena_Fox
 */
public class LoginService implements Serializable{
    // для работы с маппером пользователя
    UserGateway gateway = new UserGateway();
    String curLogin = "";
    String curPass = "";
    
    public LoginService() {
        
    }
    
    public static boolean isUserAdmin(UUID currentUsrId){
        UserGateway map = new UserGateway();
       // проверяем - обладает ли правами администратора человек
        return map.isAdminUser(currentUsrId);
    }
   
   
    
    /**
     * Бизнес - Транзакция о создании нового пользователя
     * @param login
     * @param pass 
     * @return id нового пользователя
     */
    public UUID createUser(String login, String pass){
        return UserLogic.createUser(login, pass);
    }
        
    /** 
     * проверяем - существует ли пользователь с такими логином
     * @param login
     * @return true - если существует
     */
    public boolean checkLogin(String login){
        if(gateway.loginExists(login))  return true;           
        return false;
    }
    
    /**
     * Проверяет наличие в БД пары лоигн-пароль
     * @param login
     * @param pass
     * @return true - если такая пара есть
     */
    public UUID checkLoginPass(String login, String pass){
        System.out.println(pass);
       
            UUID usrId = gateway.checkLoginPass(login, pass);
            if(usrId!=null) {
                curLogin = login;
                curPass = pass;
                return usrId;
            }           
       
        return null;
    }
}
