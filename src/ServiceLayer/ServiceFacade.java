package ServiceLayer;

import com.eaio.uuid.UUID;
import java.util.Date;
import java.util.Map;

/**
 * Класс, представляющий из себя Фасад к классам сервисного слоя
 * 
 * @author Alena_Fox
 */
public class ServiceFacade {
    private LoginService loginService;

    /**
     * Конструктор класса-фасада. Создает объект только класса LoginService - 
     * тк остальные сервисные классы доступны по статическим методам.
     * 
     * @param loginService
     */
    public ServiceFacade() {
        loginService = new LoginService();       
    }
    
    /**
     * Методы класса-сервиса "AddNewProductService"
     */
    public void addNewProduct(String name, Boolean isProduct, String kkal, 
             String prot,  String fat, String carb){
        AddNewProductService.addNewProduct(name, isProduct, kkal, prot, fat, carb);
    }
    /**
     * Методы класса-сервиса "LoginService"
     */
    public boolean isUserAdmin(UUID currentUsrId){
        return loginService.isUserAdmin(currentUsrId);
    }
    public UUID createUser(String login, String pass){
        return loginService.createUser(login, pass);
    }
    public boolean checkLogin(String login){
        return loginService.checkLogin(login);
    }
    public UUID checkLoginPass(String login, String pass){
        return loginService.checkLoginPass(login, pass);
    }
    /**
     * Методы класса-сервиса "NamesOfProdExService"
     */
    public String[] getProductNames(){
        return NamesOfProdExService.getProductNames();               
    }
    public String[] getExcercisesNames(){
        return NamesOfProdExService.getExcercisesNames();
    }
    /**
     * Методы класса-сервиса "RecordsService"
     */
    public String[][] getDiary(UUID usrID){
        return RecordsService.getDiary(usrID);
    }
    public Map<Integer, Object> getKkalAtThisDay(){
        return RecordsService.getKkalAtThisDay();
    }
    public String[][] getRecordsByDay(UUID usrID, Object dateVal){
        return RecordsService.getRecordsByDay(usrID, dateVal);
    }
    public int addNewRecord(Date realDate, UUID usrId, Object typeOfRec,
            String selName, String num ){
        return RecordsService.addNewRecord(realDate, usrId, typeOfRec, selName, num);
    }
    /**
     * Методы класса-сервиса "UserService"
     */
    public String calcBZU(int isMale, String a, int exLevel, int type,
            String  h, String  w){
        return UserService.calcBZU(isMale, a, exLevel, type, h, w);
    }
    public String[] calcIMT(String h, String w){
        return UserService.calcIMT(h, w);
    }
    public String calcKkal(UUID usrid, int isMale, String a, int exLevel,
            String h, String w){
        return UserService.calcKkal(usrid, isMale, a, exLevel, h, w);
    }
    public Map<Integer,Object> checkKkal(UUID usrId, Double kkal){
        return UserService.checkKkal(usrId, kkal);
    }
}
