package ServiceLayer;

import Entities.Products;
import DataLayer.ProductsGateway;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Класс, который берет json файл с данными о продукте\напитке и 
 * добавляет данные из него в нашу БД
 * @author Alena_Fox
 */
public class UploadJsonFileToDB {
    public static String PRODUCT_PARAM="Продукт";
    public static String PROTEIN_PARAM="Белки";
    public static String FAT_PARAM="Жиры";
    public static String CARBS_PARAM="Углеводы";
    public static String KKAL_PARAM="ккал";
    
    static ProductsGateway mapper = new ProductsGateway();
     /**
      * Файл json с содержимым типа:
        {"Продукт":"Вода","Белки":"0","Жиры":"0","Углеводы":"0,00","ккал":"0"}
        {"Продукт":"Брынза","Белки":"17,90","Жиры":"20,10","Углеводы":"0,00","ккал":"260,00"}
      * Парсится и кладется в нашу БД (в таблицу продукты).
      * @param filename - полное имя к файлу типа json
      */
    public static void readJsonFile(String filename){
        try {
            // открываем файл для чтения в текущей кодировке
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "windows-1251"));
            String line;
            while ((line = fileReader.readLine()) != null) { // считываем по строке
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(line);
                JSONObject jsonObj = (JSONObject) obj;
                String prodName = (String) jsonObj.get(PRODUCT_PARAM);
               // чтобы парсить числа с запятыми (12,5 или 17,87)
                double prot = Double.parseDouble(((String)jsonObj.get(PROTEIN_PARAM)).replaceAll(",", "."));
                double fat = Double.parseDouble(((String)jsonObj.get(FAT_PARAM)).replaceAll(",", "."));
                double carb = Double.parseDouble(((String)jsonObj.get(CARBS_PARAM)).replaceAll(",", "."));
                double kkal = Double.parseDouble(((String)jsonObj.get(KKAL_PARAM)).replaceAll(",", "."));
                // создаем продукт. По умолчанию будет все едой (не напитком)
                Products product = new Products(prodName, true, (int)kkal, (int)prot, (int)fat, (int)carb);
               try{
                   mapper.checkExistProductAndInsertOrUpdateIt(product); // если продукт уже существует - то его обновляем
                   
               }catch(Exception e){}
                
             }
        } catch (Exception ex) {
            Logger.getLogger(UploadJsonFileToDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//   // пример использования метода
// public static void main(String[] args) {
//        readJsonFile("D:\\products.json");
//    }
}