package ServiceLayer;

import Entities.Exercise;
import Entities.Products;

import DataLayer.ExercisesGateway;
import DataLayer.ProductsGateway;

import java.util.List;

/**
 *
 * @author Alena_Fox
 */
public class NamesOfProdExService {
    public static String[] getProductNames(){
            ProductsGateway mapper = new ProductsGateway();
            List<Products> findAll = mapper.findAll();
            int len = findAll.size();
            // создаем список названий продуктов
            String[] products = new String[len];
            // заполняем список 
            for(Products prod:findAll){
                products[len-1] = prod.getName();
                len--;
            }
            return products;       
    }
    
    public static String[] getExcercisesNames(){
        try {
            ExercisesGateway mapper = new ExercisesGateway();
            List<Exercise> findAll = mapper.findAll();
            int len = findAll.size();
            // создаем список названий продуктов
            String[] exs = new String[len];
            // заполняем список 
            for(Exercise ex:findAll){
                exs[len-1] = ex.getName();
                len--;
            }
            return exs;
        } catch (Exception ex) {
            return null;
        }       
    }
}
