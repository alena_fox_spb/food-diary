/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ServiceLayer;

import BusinessLogic.ProductExerciseLogic;

/**
 *
 * @author Alena_Fox
 */
public class AddNewProductService {
    public static int addNewProduct(String name, Boolean isProduct, String kkal, 
             String prot,  String fat, String carb){
      int kkal1 = 0, prot1 = 0, fat1 =0, carb1 = 0;
        try{
            // проверяем - что введено число
            if(kkal!=null && !kkal.equals("")) kkal1 = Integer.parseInt(kkal);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (kkal1<0) throw new NumberFormatException();
            
            // проверяем - что введено число
            if(prot!=null && !prot.equals("")) prot1 = Integer.parseInt(prot);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (prot1<0) throw new NumberFormatException();
            
            // проверяем - что введено число
            if(fat!=null && !fat.equals("")) fat1 = Integer.parseInt(fat);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (fat1<0) throw new NumberFormatException();
            
            // проверяем - что введено число
            if(carb!=null && !carb.equals("")) carb1 = Integer.parseInt(carb);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (carb1<0) throw new NumberFormatException();
            
        } catch(NumberFormatException e){
            return -1;
       }        
        ProductExerciseLogic.addNewProduct(name, isProduct, kkal1, prot1, fat1, carb1);
        return 0;
    }
    
}
