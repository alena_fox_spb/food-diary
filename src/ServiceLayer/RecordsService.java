package ServiceLayer;

import BusinessLogic.RecordLogic;

import com.eaio.uuid.UUID;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
/**
 *
 * @author Alena_Fox
 */
public class RecordsService {
    
    /**
     * Метод вызывает в себе маппер, который получает все записи пользователя по его uid. 
     * @return массив строк, пригодный для отображения в таблице главной формы
     */
    public static String[][] getDiary(UUID usrID){          
        return RecordLogic.getDiary(usrID);
    }
    
    /**
     * Возвращает объект из бизнес-логики, отвечающий за подсчет калорийности в день у пользователя
     * @return 
     */
    public static Map<Integer, Object> getKkalAtThisDay(){
        return RecordLogic.checkKkal;
    }
    /**
     * Метод выполняет транзакцию "найти все записи пользователя по конкретному дню"
     * @param usrID
     * @param dateVal
     * @return 
     */
    public static String[][] getRecordsByDay(UUID usrID, Object dateVal){    
        if(usrID==null||dateVal==null) return null;
        // проверки параметров
        String dateString = dateVal.toString();                                                      
        //вид записи о времени: Tue May 06 00:00:00 MSK 2014
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date= new Date();
        try {
            date = format.parse(dateString);
        } catch (ParseException ex) {
            return null;
        }  
      // транзакция - получаем записи пользователя по конкретному дню
      return RecordLogic.getRecordsByDay(usrID, date);
    }
   
    /**
     * Метод, вызываюший транзакцию "добавить запись в дневник"
     * @param realDate
     * @param usrId
     * @param typeOfRec
     * @param selName
     * @param num 
     */
    public static int addNewRecord(Date realDate, UUID usrId, Object typeOfRec,
            String selName, String num ){
        double count = 0;
        try{
            // проверяем - что введено число
            if(num!=null && !num.equals("")) count = Double.parseDouble(num);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (count<0) throw new NumberFormatException();
        } catch(NumberFormatException e){
            return -1;
        }
        // непосредственно "добавляем запись"
        RecordLogic.addNewRecord(realDate, usrId, typeOfRec, selName, count);
        return 0;
    }
}
