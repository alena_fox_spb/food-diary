/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ServiceLayer;

import BusinessLogic.UserLogic;
import com.eaio.uuid.UUID;
import java.util.Map;

/**
 *
 * @author Alena_Fox
 */
public class UserService {
    
    /**
     * По получаемым параметрам с помощью бизнес-логикик 
     * считает БЖУ и возращает его в формате String
     * @param isMale
     * @param age
     * @param exLevel
     * @param type
     * @param height
     * @param weight
     * @return 
     */
    public static String calcBZU(int isMale, String a, int exLevel, int type,
            String  h, String  w){
       
        double weight = 0, height = 0;
        int age =0;
        try{
            // проверяем - что введено число
            if(w!=null && !w.equals("")) weight = Double.parseDouble(w);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (weight<0) throw new NumberFormatException();
            
            // проверяем - что введено число
            if(h!=null && !h.equals("")) height = Double.parseDouble(h);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (height<0) throw new NumberFormatException();
            
            // проверяем - что введено число
            if(a!=null && !a.equals("")) age = Integer.parseInt(a);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (age<0) throw new NumberFormatException();
        } catch(NumberFormatException e){
            return null;
        }
        // вычисляем
        return UserLogic.calcBZU(isMale, age, exLevel, type, (int)height, (int)weight);
    }
    
    public static String[] calcIMT(String h, String w){
       double weight = 0, height = 0;
        try{
            // проверяем - что введено число
            if(w!=null && !w.equals("")) weight = Double.parseDouble(w);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (weight<0) throw new NumberFormatException();
            
            // проверяем - что введено число
            if(h!=null && !h.equals("")) height = Double.parseDouble(h);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (height<0) throw new NumberFormatException();
        } catch(NumberFormatException e){
            return null;
        }
        // вычисляем 
        return UserLogic.calcIMT((int)height, (int)weight);
    }
    /**
     * Рассчитывается калорийность и обновляются данные о пользователе (через маппер)
     * @param isMale
     * @param age
     * @param exLevel
     * @param height
     * @param weight
     * @return 
     */
    public static String calcKkal(UUID usrid, int isMale, String a, int exLevel,
            String h, String w){
        double weight = 0, height = 0;
        int age =0;
        try{
            // проверяем - что введено число
            if(w!=null && !w.equals("")) weight = Double.parseDouble(w);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (weight<0) throw new NumberFormatException();
            
            // проверяем - что введено число
            if(h!=null && !h.equals("")) height = Double.parseDouble(h);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (height<0) throw new NumberFormatException();
            
            // проверяем - что введено число
            if(a!=null && !a.equals("")) age = Integer.parseInt(a);
            else throw new NumberFormatException();
            // проверяем что оно положительное
            if (age<0) throw new NumberFormatException();
        } catch(NumberFormatException e){     
            return null;
        }
        // вычисляем 
        return UserLogic.calcKkal(usrid, isMale, age, exLevel, (int)height, (int)weight);
    }
    /**
     * Обращается к бизнес-логике для определения - переел ли человек сегодня или нет
     * Возвращает мапу - первый объект которой - строка с результатом,
     * второй объект - boolean - превышен лимит или нет
     * @param usr
     * @return 
     */
    public static Map<Integer,Object> checkKkal(UUID usrId, Double kkal){      
        return UserLogic.checkKkal(usrId, kkal);
    }
}
