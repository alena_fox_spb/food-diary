/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ServiceLayer;

import com.eaio.uuid.UUID;
import java.util.Map;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alena_Fox
 */
public class UserServiceTest {
    
    public UserServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Test ServiceLayer.UserServiceTest.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Test ServiceLayer.UserServiceTest.java - after class\n");
    }

    /**
     * Test of calcBZU method, of class UserService.
     */
    @Test
    public void testCalcBZU() {
        System.out.println("calcBZU");
        int isMale = 0;
        String a = "";
        int exLevel = 0;
        int type = 0;
        String h = "";
        String w = "";
        String expResult = null;
        String result = UserService.calcBZU(isMale, a, exLevel, type, h, w);
        assertEquals(expResult, result);
    }

    /**
     * Test of calcIMT method, of class UserService.
     */
    @Test
    public void testCalcIMT() {
        System.out.println("calcIMT");
        String h = "";
        String w = "";
        String[] expResult = null;
        String[] result = UserService.calcIMT(h, w);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of calcKkal method, of class UserService.
     */
    @Test
    public void testCalcKkal() {
        System.out.println("calcKkal");
        UUID usrid = null;
        int isMale = 0;
        String a = "";
        int exLevel = 0;
        String h = "";
        String w = "";
        String expResult = null;
        String result = UserService.calcKkal(usrid, isMale, a, exLevel, h, w);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkKkal method, of class UserService.
     */
    @Test
    public void testCheckKkal() {
        System.out.println("checkKkal");
        UUID usrId = null;
        Double kkal = null;
        Map<Integer, Object> expResult = null;
        Map<Integer, Object> result = UserService.checkKkal(usrId, kkal);
        assertEquals(expResult, result);
    }
    
}
