/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ServiceLayer;

import com.eaio.uuid.UUID;
import java.util.Date;
import java.util.Map;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alena_Fox
 */
public class RecordsServiceTest {
    
    public RecordsServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Test ServiceLayer.RecordsServiceTest.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Test ServiceLayer.RecordsServiceTest.java - after class\n");
    }

    /**
     * Test of getDiary method, of class RecordsService.
     */
    @Test
    public void testGetDiary() {
        System.out.println("getDiary");
        UUID usrID = null;
        String[][] expResult = null;
        String[][] result = RecordsService.getDiary(usrID);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getKkalAtThisDay method, of class RecordsService.
     */
    @Test
    public void testGetKkalAtThisDay() {
        System.out.println("getKkalAtThisDay");
        Map<Integer, Object> expResult = null;
        Map<Integer, Object> result = RecordsService.getKkalAtThisDay();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRecordsByDay method, of class RecordsService.
     */
    @Test
    public void testGetRecordsByDay() {
        System.out.println("getRecordsByDay");
        UUID usrID = null;
        Object dateVal = null;
        String[][] expResult = null;
        String[][] result = RecordsService.getRecordsByDay(usrID, dateVal);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of addNewRecord method, of class RecordsService.
     */
    @Test
    public void testAddNewRecord() {
        System.out.println("addNewRecord");
        Date realDate = null;
        UUID usrId = null;
        Object typeOfRec = null;
        String selName = "";
        String num = "";
        int expResult = -1;
        int result = RecordsService.addNewRecord(realDate, usrId, typeOfRec, selName, num);
        assertEquals(expResult, result);
    }
    
}
