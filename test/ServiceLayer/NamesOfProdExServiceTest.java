/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ServiceLayer;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alena_Fox
 */
public class NamesOfProdExServiceTest {
    
    public NamesOfProdExServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Test ServiceLayer.NamesOfProdExServiceTest.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Test ServiceLayer.NamesOfProdExServiceTest.java - after class\n");
    }

    /**
     * Test of getProductNames method, of class NamesOfProdExService.
     */
    @Test
    public void testGetProductNames() {
        System.out.println("getProductNames");
        String[] result = NamesOfProdExService.getProductNames();
        assertNotNull(result);
   }

    /**
     * Test of getExcercisesNames method, of class NamesOfProdExService.
     */
    @Test
    public void testGetExcercisesNames() {
        System.out.println("getExcercisesNames");
        String[] result = NamesOfProdExService.getExcercisesNames();
        assertNotNull(result);
    }
    
}
