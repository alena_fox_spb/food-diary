/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ServiceLayer;

import com.eaio.uuid.UUID;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alena_Fox
 */
public class LoginServiceTest {
    
    public LoginServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Test ServiceLayer.LoginServiceTest.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Test ServiceLayer.LoginServiceTest.java - AFTER class\n");
    }

    
    /**
     * Test of createUser method, of class LoginService.
     */
    @Test
    public void testCreateUser() {
        System.out.println("createUser");
        String login = "";
        String pass = "";
        LoginService instance = new LoginService();
        UUID expResult = null;
        UUID result = instance.createUser(login, pass);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkLogin method, of class LoginService.
     */
    @Test
    public void testCheckLogin() {
        System.out.println("checkLogin");
        String login = "";
        LoginService instance = new LoginService();
        boolean expResult = false;
        boolean result = instance.checkLogin(login);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkLoginPass method, of class LoginService.
     */
    @Test
    public void testCheckLoginPass() {
        System.out.println("checkLoginPass");
        String login = "";
        String pass = "";
        LoginService instance = new LoginService();
        UUID expResult = null;
        UUID result = instance.checkLoginPass(login, pass);
        assertEquals(expResult, result);
    }
    
}
