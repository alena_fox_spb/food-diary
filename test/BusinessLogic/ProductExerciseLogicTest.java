package BusinessLogic;

import DataLayer.ProductsGateway;
import Entities.Products;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Alena_Fox
 */
public class ProductExerciseLogicTest {
    
    public ProductExerciseLogicTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Test ProductExerciseLogic.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {        
        System.out.println("Test ProductExerciseLogic.java - after class\n");
    }

    /**
     * Test of addNewProduct method, of class ProductExerciseLogic.
     */
    @Test
    public void testAddNewProduct() {
        System.out.println("ProductExerciseLogic.java - 'addNewProduct' method");
        String name = "Вода";
        Boolean isProduct = true;
        int kkal = 0;
        int prot = 0;
        int fat = 0;
        int carb = 0;
        
        ProductExerciseLogic.addNewProduct(name, isProduct, kkal, prot, fat, carb);
        
        // проверяем - добавился\обновился ли этот продукт - 
        ProductsGateway gate = new ProductsGateway();
        try {
            Products prod = gate.findByName(name);
            assertEquals(name, prod.getName());
            assertEquals(isProduct, prod.isIsFood());
            assertEquals(kkal, prod.getKkal());
            assertEquals(fat, prod.getFats());
            assertEquals(carb, prod.getCarbons());
        } catch (Exception ex) {
            Logger.getLogger(ProductExerciseLogicTest.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
}
