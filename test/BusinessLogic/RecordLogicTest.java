package BusinessLogic;

import com.eaio.uuid.UUID;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alena_Fox
 */
public class RecordLogicTest {
    
    public RecordLogicTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       System.out.println("Test  BusinessLogic.RecordLogic.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
         System.out.println("Test  BusinessLogic.RecordLogic.java - after class\n");
    }

    /**
     * Test of getDiary method, of class RecordLogic.
     */
    @Test
    public void testGetDiary() {
        System.out.println("getDiary");
        UUID usrID = null;
        String[][] expResult = null;
        String[][] result = RecordLogic.getDiary(usrID);
        assertArrayEquals(expResult, result);        
    }

    /**
     * Test of getDiaryToExport method, of class RecordLogic.
     */
    @Test
    public void testGetDiaryToExport() {
        System.out.println("getDiaryToExport");
        UUID usrID = null;
        String[][] expResult = null;
        String[][] result = RecordLogic.getDiaryToExport(usrID);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getRecordsByDay method, of class RecordLogic.
     */
    @Test
    public void testGetRecordsByDay() {
        System.out.println("getRecordsByDay");
        UUID usrID = null;
        Date date = null;
        String[][] expResult = null;
        String[][] result = RecordLogic.getRecordsByDay(usrID, date);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of addNewRecord method, of class RecordLogic.
     */
    @Test
    public void testAddNewRecord() {
        System.out.println("addNewRecord");
        Date realDate = null;
        UUID usrId = null;
        Object typeOfRec = null;
        String selName = "";
        double numOf = 0.0;
        RecordLogic.addNewRecord(realDate, usrId, typeOfRec, selName, numOf);
        
    }
    
}
