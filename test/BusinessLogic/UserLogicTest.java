package BusinessLogic;

import Entities.User;
import com.eaio.uuid.UUID;
import java.util.Map;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alena_Fox
 */
public class UserLogicTest {
    
    public UserLogicTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       System.out.println("Test BusinessLogic.UserLogicTest.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Test BusinessLogic.UserLogicTest.java - after class\n");
    }

    /**
     * Test of createUser method, of class UserLogic.
     */
    @Test
    public void testCreateUser() {
        System.out.println("createUser");
        String login = "";
        String pass = "";
        UUID expResult = null;
        UUID result = UserLogic.createUser(login, pass);
        assertEquals(expResult, result);
    }

    /**
     * Test of calcBZU method, of class UserLogic.
     */
    @Test
    public void testCalcBZU() {
        System.out.println("calcBZU");
        int isMale = 0;
        int age = 21;
        int exLevel = 0;
        int type = 0;
        int height = 178;
        int weight = 68;
        String expResult = "Белки: 119.7528-146.00279999999998\nЖиры: 35.48231111111111-43.26008888888889\nУглеводы: 199.588-243.338\n";
        String result = UserLogic.calcBZU(isMale, age, exLevel, type, height, weight);
        assertEquals(expResult, result);
    }

    /**
     * Test of calcKkal method, of class UserLogic.
     */
    @Test
    public void testCalcKkal() {
        System.out.println("calcKkal");
        UUID usrid = null;
        int isMale = 0;
        int age = 0;
        int exLevel = 0;
        int height = 0;
        int weight = 0;
        String expResult = null;
        String result = UserLogic.calcKkal(usrid, isMale, age, exLevel, height, weight);
       
        assertEquals(expResult, result);
    }

    /**
     * Test of checkKkal method, of class UserLogic.
     */
    @Test
    public void testCheckKkal() {
        System.out.println("checkKkal");
        UUID usrId = null;
        Double kkal = null;
        Map<Integer, Object> expResult = null;
        Map<Integer, Object> result = UserLogic.checkKkal(usrId, kkal);
        assertEquals(expResult, result);
    }

    /**
     * Test of CalcIMT method, of class UserLogic.
     */
    @Test
    public void testCalcIMT_User() {
        System.out.println("CalcIMT");
        User usr = new User("", "");
        usr.setWeight(68);
        usr.setHeight(178);
        double expResult = 21.461936624163616;
        double result = UserLogic.CalcIMT(usr);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of CalcKkalPerDay method, of class UserLogic.
     */
    @Test
    public void testCalcKkalPerDay() {
        System.out.println("CalcKkalPerDay");
        User usr = new User("", "");
        usr.setDataAboutUser(true, 21, 178, 68, 0);
        double expResult =  2099.916;
        double result = UserLogic.CalcKkalPerDay(usr);
        assertEquals(expResult, result, 0.0);
    }

}
