/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DataLayer;

import Entities.Exercise;
import com.eaio.uuid.UUID;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alena_Fox
 */
public class ExercisesGatewayTest {
    
    public ExercisesGatewayTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Test DataLayer.ExercisesGatewayTest.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {        
        System.out.println("Test DataLayer.ExercisesGatewayTest.java - before class\n");
    }

    /**
     * Test of insert method, of class ExercisesGateway.
     */
    @Test
    public void testInsert() throws Exception {
        System.out.println("insert");
        Exercise ex = null;
        ExercisesGateway instance = new ExercisesGateway();
        int expResult = -1;
        int result = instance.insert(ex);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByName method, of class ExercisesGateway.
     */
    @Test
    public void testFindByName() {
        System.out.println("findByName");
        String name = "";
        ExercisesGateway instance = new ExercisesGateway();
        Exercise expResult = null;
        Exercise result = instance.findByName(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByGuId method, of class ExercisesGateway.
     */
    @Test
    public void testFindByGuId() {
        System.out.println("findByGuId");
        UUID uniqueID = null;
        ExercisesGateway instance = new ExercisesGateway();
        Exercise expResult = null;
        Exercise result = instance.findByGuId(uniqueID);
        assertEquals(expResult, result);
    }
    
}
