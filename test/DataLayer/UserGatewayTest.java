/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DataLayer;

import Entities.User;
import com.eaio.uuid.UUID;
import java.sql.ResultSet;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alena_Fox
 */
public class UserGatewayTest {
    
    public UserGatewayTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        System.out.println("Test DataLayer.UserGatewayTest.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Test DataLayer.UserGatewayTest.java - after class\n");
    }

    /**
     * Test of insertNewUsr method, of class UserGateway.
     */
    @Test
    public void testInsertNewUsr() throws Exception {
        System.out.println("insertNewUsr");
        User usr = null;
        UserGateway instance = new UserGateway();
        int expResult = -1;
        int result = instance.insertNewUsr(usr);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateUsrInfo method, of class UserGateway.
     */
    @Test
    public void testUpdateUsrInfo() throws Exception {
        System.out.println("updateUsrInfo");
        User usr = null;
        UserGateway instance = new UserGateway();
        int expResult = -1;
        int result = instance.updateUsrInfo(usr);
        assertEquals(expResult, result);
    }

    /**
     * Test of insertLoginPass method, of class UserGateway.
     */
    @Test
    public void testInsertLoginPass() {
        System.out.println("insertLoginPass");
        User usr = null;
        UserGateway instance = new UserGateway();
        int expResult = -1;
        int result = instance.insertLoginPass(usr);
        assertEquals(expResult, result);
    }

    /**
     * Test of findbyID method, of class UserGateway.
     */
    @Test
    public void testFindbyID() throws Exception {
        System.out.println("findbyID");
        UUID id = null;
        UserGateway instance = new UserGateway();
        User expResult = null;
        User result = instance.findbyID(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of isAdminUser method, of class UserGateway.
     */
    @Test
    public void testIsAdminUser() {
        System.out.println("isAdminUser");
        UUID usrId = null;
        UserGateway instance = new UserGateway();
        boolean expResult = false;
        boolean result = instance.isAdminUser(usrId);
        assertEquals(expResult, result);
    }

    /**
     * Test of findIdByUserName method, of class UserGateway.
     */
    @Test
    public void testFindIdByUserName() {
        System.out.println("findIdByUserName");
        String name = "";
        UserGateway instance = new UserGateway();
        UUID expResult = null;
        UUID result = instance.findIdByUserName(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of findLoginByUserID method, of class UserGateway.
     */
    @Test
    public void testFindLoginByUserID() {
        System.out.println("findLoginByUserID");
        UUID id = null;
        UserGateway instance = new UserGateway();
        ResultSet expResult = null;
        ResultSet result = instance.findLoginByUserID(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of loginExists method, of class UserGateway.
     */
    @Test
    public void testLoginExists() {
        System.out.println("loginExists");
        String login = "";
        UserGateway instance = new UserGateway();
        boolean expResult = false;
        boolean result = instance.loginExists(login);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkLoginPass method, of class UserGateway.
     */
    @Test
    public void testCheckLoginPass() {
        System.out.println("checkLoginPass");
        String login = "";
        String pass = "";
        UserGateway instance = new UserGateway();
        UUID expResult = null;
        UUID result = instance.checkLoginPass(login, pass);
        assertEquals(expResult, result);
    }
    
}
