/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DataLayer;

import Entities.Products;
import com.eaio.uuid.UUID;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alena_Fox
 */
public class ProductsGatewayTest {
    
    public ProductsGatewayTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Test DataLayer.ProductsGatewayTest.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Test DataLayer.ProductsGatewayTest.java - after class\n");
    }

    /**
     * Test of checkExistProductAndInsertOrUpdateIt method, of class ProductsGateway.
     */
    @Test
    public void testCheckExistProductAndInsertOrUpdateIt() {
        System.out.println("checkExistProductAndInsertOrUpdateIt");
        Products prod = null;
        ProductsGateway instance = new ProductsGateway();
        int expResult = -1;
        int result = instance.checkExistProductAndInsertOrUpdateIt(prod);
        assertEquals(expResult, result);
    }

    /**
     * Test of insert method, of class ProductsGateway.
     */
    @Test
    public void testInsert() {
        System.out.println("insert");
        Products prod = null;
        ProductsGateway instance = new ProductsGateway();
        int expResult = -1;
        int result = instance.insert(prod);
        assertEquals(expResult, result);
    }

    /**
     * Test of update method, of class ProductsGateway.
     */
    @Test
    public void testUpdate() {
        System.out.println("update");
        Products prod = null;
        ProductsGateway instance = new ProductsGateway();
        int expResult = -1;
        int result = instance.update(prod);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByName method, of class ProductsGateway.
     */
    @Test
    public void testFindByName() {
        System.out.println("findByName");
        String name = null;
        ProductsGateway instance = new ProductsGateway();
        Products expResult = null;
        Products result = instance.findByName(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByGuId method, of class ProductsGateway.
     */
    @Test
    public void testFindByGuId() {
        System.out.println("findByGuId");
        UUID uniqueID = null;
        ProductsGateway instance = new ProductsGateway();
        Products expResult = null;
        Products result = instance.findByGuId(uniqueID);
        assertEquals(expResult, result);
    }
    
}
