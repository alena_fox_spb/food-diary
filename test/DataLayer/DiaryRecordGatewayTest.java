/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DataLayer;

import Entities.RecordInDairy;
import com.eaio.uuid.UUID;
import java.util.Date;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alena_Fox
 */
public class DiaryRecordGatewayTest {
    
    public DiaryRecordGatewayTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Test DataLayer.DiaryRecordGatewayTest.java - before class\n");
    }
    
    @AfterClass
    public static void tearDownClass() {        
        System.out.println("Test DataLayer.DiaryRecordGatewayTest.java - after class\n");
    }

    /**
     * Test of insert method, of class DiaryRecordGateway.
     */
    @Test
    public void testInsert() throws Exception {
        System.out.println("insert");
        RecordInDairy rec = null;
        UUID userID = null;
        DiaryRecordGateway instance = new DiaryRecordGateway();
        int expResult = -1;
        int result = instance.insert(rec, userID);
        assertEquals(expResult, result);
    }

    /**
     * Test of findbyRecID method, of class DiaryRecordGateway.
     */
    @Test
    public void testFindbyRecID() throws Exception {
        System.out.println("findbyRecID");
        UUID recID = null;
        DiaryRecordGateway instance = new DiaryRecordGateway();
        RecordInDairy expResult = null;
        RecordInDairy result = instance.findbyRecID(recID);
        assertEquals(expResult, result);
    }

    /**
     * Test of findbyAllRecordsByUsrID method, of class DiaryRecordGateway.
     */
    @Test
    public void testFindbyAllRecordsByUsrID() {
        System.out.println("findbyAllRecordsByUsrID");
        UUID usrID = null;
        DiaryRecordGateway instance = new DiaryRecordGateway();
        List<RecordInDairy> expResult = null;
        List<RecordInDairy> result = instance.findbyAllRecordsByUsrID(usrID);
        assertEquals(expResult, result);
    }

    /**
     * Test of findRecordsByDate method, of class DiaryRecordGateway.
     */
    @Test
    public void testFindRecordsByDate() {
        System.out.println("findRecordsByDate");
        UUID usrID = null;
        Date date = null;
        DiaryRecordGateway instance = new DiaryRecordGateway();
        List<RecordInDairy> expResult = null;
        List<RecordInDairy> result = instance.findRecordsByDate(usrID, date);
        assertEquals(expResult, result);
    }
    
}
